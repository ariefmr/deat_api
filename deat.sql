CREATE TABLE users(
    id_users int primary key auto_increment,
    users_username varchar(200) not null,
    users_password varchar(200) not null,
    users_email varchar(200) not null,
    users_reg_id varchar(200) not null,
    users_phone_type varchar(200),
    users_dob date,
    users_biodata text,
    users_profile_photo text,
    users_background_photo text,
    users_rating int,
    users_gender varchar(200),
);



CREATE TABLE collection_rules(
    id_collection_rules int primary key auto_increment,
    collection_rules_name varchar(200),
    collection_rules_description text,
    collection_rules_status_publish int,
    users_id int not null,
    foreign key (users_id) references users(id_users) on delete cascade on update cascade
);

CREATE TABLE diet_rules(
    id_diet_rules int primary key auto_increment,
    diet_rules_title varchar(200) not null,
    diet_rules_description text,
    diet_rules_type int,
    diet_rules_time time,
    users_id int not null,
    collection_rules_id int not null,
    foreign key (collection_rules_id) references collection_rules(id_collection_rules) on delete cascade on update cascade,
    foreign key (users_id) references users(id_users) on delete cascade on update cascade
);

CREATE TABLE bookmark(
    id_bookmark int primary key auto_increment,
    bookmark_implement int,
    bookmark_date timestamp,
    bookmark_target_id int not null,
    users_id int not null,
    foreign key (users_id) references users(id_users) on delete cascade on update cascade,
    foreign key (bookmark_target_id) references users(id_users) on delete cascade on update cascade
);


CREATE TABLE users_photo(
    id_users_photo int primary key auto_increment,
    users_photo_name varchar(200) not null,
    users_photo_description text,
    users_photo_upload_date timestamp,
    users_photo_path text not null,
    users_id int not null,
    foreign key (users_id) references users(id_users) on delete cascade on update cascade
);

CREATE TABLE news_feed(
    id_news_feed int primary key auto_increment,
    news_feed_message text,
    news_feed_time datetime not null,
    news_feed_target_id int not null,
    users_id int not null,
    foreign key (users_id) references users(id_users) on delete cascade on update cascade,
    foreign key (news_feed_target_id) references users(id_users) on delete cascade on update cascade
);

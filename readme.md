#Deat API Development Guideline

Bagi setiap teman - teman yang menambahkan fitur dan dapat mempengaruhi keseluruhan projek, diharapkan untuk menambahkan *guideline* atau informasi mengenai pengaturan dan aturan tersebut di dokumen ini. Gunakan ReText untuk melakukan editing dalam membuat dokumen markdown.

### by Arief

database pakai deat.sql
nama database deat_api


### by Ridwan

Masalah Routing URL:

* URL yang digunakan untuk API memiliki format base_url()/api/fitur/fungsional misal: http://deat.devnila.com/api/users/register
* fitur dasar setiap fitur adalah CRUDS (create, read, update, detail, searching). Fungsional diluar fitur dasar tersebut dapat dibuat setelah mendiskusikan fitur dengan developer mobile
* URL untuk console harus base_url/api/fitur
* silahkan lihat contoh pembuatan routing URL di **application/config/routes.php**

Masalah *view* untuk console fitur:

* nama dari file view console adalah **(fitur)_console.php**
* setiap form diberi judul dengan nama fungsional yang ada di controller
* setelah judul harus menerakan URL dari fungsional tersebut (tentunya yang sudah diset di routes.php)
* form harus memiliki method POST dan target blank
* form harus memiliki field users_token
* label dan nama field dari setiap form harus sesuai sama dengan nama field di tabel
* form memilik tombol submit dan reset
* field yang wajib diisi harus ditandai merah atau menggunakan bintang
* silahkan lihat contoh pembuatan console di
 **application/modules/users/views/users_console.php**

  
Masalah *controllers* untuk console fitur:

* harus ada validasi jika ada *field* yang wajib diisi
* jika kembalian JSON banyak field, harus memiliki field message, data_name, num_data, result, data, dan users_token
* jika kembalian JSON hanya satu field, harus memiliki field message, data_name, num_data, result, users_token, dan setiap field tabel yang didapat
* kembalian JSON harus menggunakan JSON_PRETTY_PRINT
* message adalah pesan yang dapat Anda sampaikan kepada developer mobile
* data_name adalah nama dari fungsional atau fungsi yang ada di controller
* num_data adalah jumlah kembalian data JSON
* result adalah status yang berisi gagal atau berhasil
* data adalah kumpulan hasil query yang dikembalikan
* users_token sementara ini dikosongkan dulu
* silahkan lihat contoh pembuatan controller di **application/modules/users/controllers/main.php
* jangan lupa untuk mengatur routing URL di routes.php

Masalah model:

* Usahakan jangan ada SQL di controller
* setiap file model diawali dengan nama tabel
* SQL tidak ada ketentuan
* setiap fitur yang memiliki kembalian data banyak, harus dipaging.
* silahkan lihat contoh di **application/modules/users/models/users_model.php**

Pembagian tugas:
  
* users & news_feed, ridwan
* bookmark, zia
* diet_rules, ricki
* my_photo, arief

Bila ada yang keberatan silahkan sampaikan di issue atau lewat fb

Besok harus diupload ke hostingan, agar dapat dicoba oleh Ape.
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * Library untuk bikin form input 
 */
 
class Mkform
{
	/*  @input_text
		$attr = array( 'value' => 'default value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */
	function input_text($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		$html = '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<input id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				type="text"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				value="'.$default_value.'" >
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_textarea
		$attr = array( 'value' => 'default value', // optional
					   'rows' => '3', // wajib ada
					   'name' => 'input name', // wajib ada
					   'label' => 'text label for input' // wajib ada
	 				);
	 */
	function input_textarea($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		$html = '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<textarea id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				rows="'.$attr['rows'].'" >'.$default_value.'</textarea>
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_select
		$attr = array( 'array_opsi' => array('1' => 'Baru', '2'=> 'Lama'), // optional 
					   'value' => 'selected value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */
	function input_select($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$html = '<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select id="id_'.$attr['name'].'" name="'.$attr['name'].'" class="form-control">';
	      			foreach ($array_opsi as $value => $text) {
	      				$selected = $default_value === $value ? 'selected' : ''; 
	      				$html .= '<option value="'.$value.'">'.$text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div><!-- akhir form select '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_select
		$attr = array( 'array_opsi' => array('1' => 'Baru', '2'=> 'Lama'), // optional 
					   'value' => 'selected value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */
	function input_select2($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$html = '<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select id="id_'.$attr['name'].'" name="'.$attr['name'].'" style="width: 100% !important;">
	      			<option value="0">-</option>';
	      			foreach ($array_opsi as $item) {
	      				$selected = $default_value === $item->id ? 'selected' : ''; 
	      				$html .= '<option value="'.$item->id.'" '.$selected.'>'.$item->text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div>';
	  	$js   = '<script>
	  				var func_'.$attr['name'].' = function() {
									  					$("#id_'.$attr['name'].'").select2();
									  				};
					s_func.push(func_'.$attr['name'].');
	  			</script>
	  			<!-- akhir form select '.$attr['name'].' -->';
	 return $html.$js;
	}

	/*  @input_checkbox
		$attr = array( 'value' => 'checked value', // Wajib ada
					   'name' => 'input name', // Wajib ada
					   'label' => 'text label for input' // Wajib ada
					   'is_checked' => '1' // 1 : checked , 0 : not checked , default is 0
	 				);
	 */
	function input_checkbox($attr)
	{
		$is_checked = '';
		if(isset($attr['is_checked']))
		{
			$is_checked = $attr['is_checked'] === $attr['value'] ? ' checked ' : '';
		}
		$html = '<input type="checkbox" id="id_'.$attr['name'].'" name="'.$attr['name'].'" value="'.$attr['value'].'" '.$is_checked.'>
        		 <label for="id_'.$attr['name'].'">'.$attr['label'].'</label>';
        return $html;
	}


	/*  @input_date
		$attr = array( 'mindate' => 'various', // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
					   'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
					   'defaultdate' => '2013-20-12', // opsi: '', tidak wajib ada
					   'placeholder' => '', // wajib ada atau '' (kosong)
					   'name' => 'input name', // wajib ada
					   'label' => 'text label for input' // wajib ada
	 				);
	 */
	function input_date($attr)
	{	

		if( !isset($attr['mindate']) || empty($attr['mindate']) )
		{
			$set_mindate = '';
		}elseif(is_array($attr['mindate'])){
			$timestamp = strtotime('-'.$attr['mindate']['time']);
			$mindate =  date('Y-m-d', $timestamp);
			$set_mindate = 'minDate: new Date("'.$mindate.'"),';
		}else{
			$set_mindate = 'new Date("'.$attr['mindate'].'"),';
		}

		if( !isset($attr['maxdate']) || empty($attr['mindate']) )
		{
			$set_maxdate = 'maxDate: new Date(),';
		}elseif(is_array($attr['maxdate'])){
			$timestamp = strtotime('+'.$attr['maxdate']['time']);
			$maxdate =  date('Y-m-d', $timestamp);
			$set_maxdate = 'minDate: new Date("'.$maxdate.'"),';
		}else{
			$set_maxdate = 'new Date("'.$attr['maxdate'].'"),';
		}

		if( !isset($attr['default_date']) || empty($attr['default_date']) )
		{
			$set_default_date = "new Date()";
		}else{
			$set_default_date = "new Date('".$attr['default_date']."')";
		}
         $html = '<div class="form-group mkform-date"><!-- mulai form date '.$attr['name'].' -->';
         $html .= '<label for="id_'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>';
         $html .=       '<div class="col-sm-8">
                         <input id="id_'.$attr['name'].'" 
                         		name="'.$attr['name'].'"
                         		type="hidden">
                         <input id="id_'.$attr['name'].'_display" 
                         			readonly="true"
                         			type="text"
                         			class="form-control"
                         			placeholder="'.$attr['placeholder'].'">
                         </div>
                       </div>';
         $js =  "<script>
                $(document).ready(function() {
		                $('#id_".$attr['name']."_display').datepicker({  
		                                                            altField: '#id_".$attr['name']."',
		                                                            altFormat: 'yy-mm-dd',
		                                                            ".$set_mindate."
		                                                            ".$set_maxdate."
		                                                            changeMonth: true,
		                                                            changeYear: true,
		                                                            dateFormat: 'dd/mm/yy',
		                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
		                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab']
		                                                         });
		                  $('#id_".$attr['name']."_display').datepicker( 'setDate', ".$set_default_date." );
		                  $('#id_".$attr['name']."_display').focus(function(){ 
		                  $('#ui-datepicker-div').css('z-index', '9999');
	                  	});";
          $js .= "});
                </script> <!-- akhir form date ".$attr['name']." -->";                      
         return $html.$js;
	}

	function input_image($attr)
	{
		// vdump($attr);
		//$default_value = isset($attr['value'])&&($attr['value']!='') ? $attr['value'] : 'assets/third_party/images/nophoto.jpg';
		if(!isset($attr['view_file'])){
			$attr['view_file'] = true;
		}
		//Menambah fasilitas disabled pada satu input_image
		$disabled = '';
		if(isset($attr['disabled']))
		{

			$disabled = $attr['disabled']==TRUE ? 'disabled' : '';

		}
		$html = '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
				 <label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
				 <div class="col-sm-12">
				 	<div id="div_'.$attr['name'].'" class="col-lg-6">';
		if($attr['view_file']==true){

				$html .= '<div class="row">';
				$count = 0;

				// echo $attr['value'][0]->path_foto;
				if(isset($attr['value'][0]->path_foto))
				{
					$foto_utama = $attr['value'][0];
					$attr['value'][0]->path_foto = 'uploads/foto_kapal/'.$attr['value'][0]->path_foto;

				}else{
					$foto_utama = (object)array( 'path_foto'=>'assets/third_party/images/nophoto.jpg');
				}
				/*vdump(array( (object)array( 'path_foto'=>'assets/third_party/images/nophoto.jpg')));
				vdump($foto_utama);
				vdump($attr['value'][0]);*/

				$html .= '<div class="col-lg-6">';
					$html .= '<a href="#" class="thumbnail" ><img src="'.base_url($foto_utama->path_foto).'"
				    		  id="id_img_'.$foto_utama->path_foto.'"
				    		  name="img_'.$foto_utama->path_foto.'"
				    		  data-src="holder.js/100%x180"
				    			></a>';
				    $html .= '</div>';

				   if($attr['value']!==NULL){
					$html .= '<div class="col-lg-6">';

					foreach($attr['value'] as $data ){

						if($count != 0){
							$html .= '<a href="#" class="thumbnail" ><img src="'.base_url('uploads/foto_kapal/'.$data->path_foto).'"
						    		  id="id_img_'.$data->path_foto.'"
						    		  name="img_'.$data->path_foto.'"
						    		  data-src="holder.js/100%x180"
						    			></a>';
						} 
						$count++;
					}
			    $html .= '</div>';
				}

				$html .= '	</div>';

		}

		/*else{
			if($default_value == 'assets/third_party/images/nophoto.jpg'){
				$default_value = '';
			}
			if(strpos($default_value, 'foto_kapal'))
			{
				
				$value = explode("/foto_kapal/", $default_value);
				$default_value = $value[1];

			}elseif (strpos($default_value, 'file_gross')) 
			{
				
				$value = explode("/file_gross/", $default_value);
				$default_value = $value[1];

			}

			$html .= $default_value;
		}		    			*/
		
		$html .=		'<input id="id_'.$attr['name'].'" 
		      				name="'.$attr['name'].'"
		      				type="file"
		      				class="form-control"
		      				'.$disabled.' >
		    		</div>
		    		</div>
	  			 </div><!-- akhir form text '.$attr['name'].' -->';
	  	return $html;
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('labels');
			$this->load->config('globals');
	}

	/** @v_form
	 * @nama_module nama folder module nya  
	 * @file_form nama file views nya
	 * @item_label index label yang akan digunakan di labels.php
	 * @add_js array berisi nama file js yang dipanggil khusus
	 * @add_cs array berisi nama file css yang dipanggil khusus
	 * @container_data array data hasil olahan di controller asal
	 */
	public function v_form($nama_module, $file_form, $item_label, $add_js = '', $add_css = '', $container_data)
	{	
		if( empty($item_label) )
		{
			$form_constants['title']['page'] = '';
			$form_constants['title']['panel'] = '';
			$form_constants['form'] = '';
		}else{
			$form_constants = $this->config->item($item_label);
		}

		$menus_constants = $this->config->item('lists');

		$global_constants = $this->config->item('app');
		$assets_paths = $this->config->item('assets_paths');

		
		$head_data['page_title'] = $form_constants['title']['page'];
		$head_data['paths'] = $assets_paths;
		$head_data['additional_js'] = empty($add_js) ? FALSE : $add_js;
		$head_data['additional_css'] = empty($add_css) ? FALSE : $add_css;
		
		$app_data['app_title'] = $global_constants['header_title'];
		$app_data['app_footer'] = $global_constants['footer_text'];
		$app_data['images_url'] = $assets_paths['devnila_images'];

		$menu_lists['menus'] = $menus_constants;

		$container_data['nama_module'] = $nama_module;
		$container_data['file_form'] = $file_form;
		$container_data['panel_title'] = $form_constants['title']['panel'];
		$container_data['form'] = $form_constants['form'];
		$container_data['constants'] = $form_constants;

		
		$this->load->view('html_head', $head_data);
		$this->load->view('div_header', $app_data);
		$this->load->view('div_menu', $menu_lists);
		$this->load->view('div_container', $container_data);
		$this->load->view('html_footer', $app_data);
	}

	public function v_view($nama_module, $file_view, $add_js, $add_css, $data)
	{
		$this->load->view($file_view);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
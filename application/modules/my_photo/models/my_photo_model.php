<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_photo_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }

    public function input_gambar($data)
    {
        
        $this->db->insert('users_photo', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $status_upload = $this->db->insert_id();
        }
        else{
            $status_upload = false;
        }

        return $status_upload;
    }
    
    public function get_all_photo($page, $limit)
    {
        $this->db->select('*');
        return $this->db->get('users_photo', $limit, $page)->result();
    }
    
    public function get_photo_by_id($id)
    {
        $this->db->where(array('id_users_photo'=>$id));
        $result = $this->db->get('users_photo')->row();
        $last_query = $this->db->last_query();
        return $result;
    }
    
	public function get_photo_by_users_id($id)
    {
        $this->db->where(array('users_id'=>$id));
        $result = $this->db->get('users_photo')->result();
        $last_query = $this->db->last_query();
        return $result;
    }
    
    public function get_users_by_account($email, $password)
    {
        $this->db->where(array('users_email'=>$email, 'users_password'=>$password));
        return $this->db->get('users')->row();
    }
    public function get_users($id)
    {
        $this->db->where(array('id_users'=>$id));
        return $this->db->get('users')->row();
    }
    
    public function get_users2($name, $email){
        $this->db->where(array('users_username'=>$name, 'users_email'=>$email));
        return $this->db->get('users')->row();
    }
    
    public function add_users($data)
    {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }
    
    public function edit_users($data, $id)
    {
        $this->db->where('id_users', $id);
        $this->db->update('users', $data);
    }
    
    public function search_users($type, $keyword)
    {
        $this->db->like($type, $keyword, 'both');
        return $this->db->get('users')->result();
    }
}

/* End of file band_model.php */
/* Location: ./application/controllers/band_model.php */

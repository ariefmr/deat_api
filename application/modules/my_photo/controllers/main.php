<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->model('my_photo_model');
        $this->load->model('users/users_model');
    }

    function index(){
        $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
        $add_css = array('select2.css', 'jquery.dataTables.css');

        $template = 'templates/page/v_form';
        $modules = 'my_photo';
        $views = 'my_photo_console';
        $labels = 'my_photo_label';

        $data['submit_form'] = 'main/functions/input_init';

        echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
    }
    
    function input()
    {   
        $array_input = $this->input->post(NULL, TRUE);

        $photo_name = $array_input['user-photo-name'];
        $photo_description = $array_input['user-photo-description'];
        $photo_upload_date = $array_input['user-photo-upload-date'];
        $users_id = $array_input['user-id'];
        // vdump($array_input, true);

        //config upload
        $config['overwrite']     = TRUE;
        $config['allowed_types'] = 'gif|jpeg|jpg|png';
        $config['file_name']     = $photo_name;
        $config['upload_path']   = 'uploads/photo_users/';
        $this->load->library('upload', $config);

        // $this->upload->initialize($config);

        $form_upload = "user-photo-path";

        if($this->upload->do_upload($form_upload))
        {
            //do_upload_foto
            $upload_foto = $this->upload->data();
            $data['users_photo_name'] = $photo_name;
            $data['users_photo_description'] = $photo_description;
            $data['users_photo_upload_date'] = $photo_upload_date;
            $data['users_photo_path'] = $upload_foto['file_name'];
            $data['users_id'] = $users_id;
            $users_profile_photo_id = $this->my_photo_model->input_gambar($data);
            $temp_data = array(
                        'users_profile_photo_id' => $data['users_photo_path']
                    );
            $this->users_model->edit_users($temp_data, $data['users_id']);

        }else{
            $error = array('error' => $this->upload->display_errors());
            vdump($error);
        }

        $temp_page = $this->input->post('page');
        $limit = $this->input->post('limit');
        $page = ($temp_page - 1) * $limit;
        $list_photo = $this->my_photo_model->get_photo_by_id($users_profile_photo_id);
        $json_data = array(
            'message' => 'Foto berhasil dimuat...',
            'data_name' => 'get_users',
            'num_data' => count($list_photo),
            'result' => 'success',
            'data' => $list_photo,
            'users_token' => ''
        );
        
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function get_photo_by_users_id()
    {
        $temp_page = $this->input->post('page');
        $id = $this->input->post('users-id');
        $list_photo = $this->my_photo_model->get_photo_by_users_id($id);
        $json_data = array(
            'message' => 'Semua users berhasil dimuat...',
            'data_name' => 'get_users',
            'num_data' => count($list_photo),
            'result' => 'success',
            'data' => $list_photo,
            'users_token' => 'c3fef41ee162b8134ca1a4f26571554b'
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function detail()
    {
        $json_data = array();
        
        /*if (empty($this->input->post('users-id'))){
            $json_data = array(
                'message' => 'Detail users gagal dimuat. ID Users tidak boleh kosong..',
                'data_name' => 'get_users',
                'num_data' => 0,
                'result' => 'failed',
            );
        }*/
        // else {
            $users = $this->my_photo_model->get_users($this->input->post('users-id', true));
            if (count($users) == 0){
                $json_data = array(
                    'message' => 'Detail users gagal dimuat. Users tidak ditemukan..',
                    'data_name' => 'get_users',
                    'num_data' => count($users),
                    'result' => 'failed',
                );

            }
            else if (count($users) > 0){
                $json_data = array(
                    'message' => 'Detail users berhasil dimuat...',
                    'data_name' => 'get_users',
                    'num_data' => count($users),
                    'result' => 'success',
                    'id_users' => $users->id_users,
                    'users_reg_id' => $users->users_reg_id,
                    'users_phone_type' => $users->users_phone_type,
                    'users_email' => $users->users_email,
                    'users_username' => $users->users_username,
                    'users_dob' => $users->users_dob,
                    'users_biodata' => $users->users_biodata,
                    'users_profile_photo' => $users->users_profile_photo,
                    'users_rating' => $users->users_rating,
                    'users_token' => '',
                );
            }
        // }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data,  JSON_PRETTY_PRINT);
    }
    
    function register()
    {
        $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('users-email', 'users-email', 'required');
        $this->form_validation->set_rules('users-username', 'users-username', 'required');
        $this->form_validation->set_rules('users-password', 'users-password', 'required');
        $this->form_validation->set_rules('users-reg-id', 'users-reg-id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $json_data = array(
                'message' => "[ERROR]: ".validation_errors(),
                'data_name' => 'register_users',
                'num_data' => 0,
                'result' => 'failed',
            );
        }
        else
        {
            $users = $this->my_photo_model->get_users2(
                                                    $this->input->post('users-username', true), 
                                                    $this->input->post('users-password', true)
                                                );
            if (count($users) > 0){
                $json_data = array(
                    'message' => 'Users sudah ada. Tidak bisa ditambahkan',
                    'data_name' => 'register_users',
                    'num_data' => count($users),
                    'result' => 'failed',
                );

            }
            else if (count($users) == 0){
                $temp_data = array(
                    'users_username' => $this->input->post('users-username'),
                    'users_password' => $this->input->post('users-password'),
                    'users_email' => $this->input->post('users-email'),
                    'users_dob' => $this->input->post('users-dob'),
                    'users_biodata' => $this->input->post('users-biodata'),
                    'users_profile_photo' => $this->input->post('users-profile-photo'),
                    'users_rating' => $this->input->post('users-rating'),
                    'users_reg_id' => $this->input->post('users-reg-id'),
                    'users_phone_type' => $this->input->post('users-phone-type'),
                    'users_token' => $this->input->post('users-token'),
                    'users_username' => $this->input->post('users-username'),
                );
                
                $users_id = $this->my_photo_model->add_users($temp_data);
                $users = $this->my_photo_model->get_users($users_id);
            
                $json_data = array(
                    'message' => 'Users berhasil terdaftar',
                    'data_name' => 'register_users',
                    'num_data' => count($users),
                    'result' => 'success',
                    'id_users' => $users->id_users,
                    'users_username' => $users->users_username,
                    'users_password' => $users->users_password,
                    'users_email' => $users->users_email,
                    'users_dob' => $users->users_dob,
                    'users_biodata' => $users->users_biodata,
                    'users_profile_photo' => $users->users_profile_photo,
                    'users_rating' => $users->users_rating,
                    'users_reg_id' => $users->users_reg_id,
                    'users_phone_type' => $users->users_phone_type,
                    'users_token' => $users->users_token,
                );
                
            }
            
            
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data,  JSON_PRETTY_PRINT);
    }
    
    function update()
    {
    
        $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('users-id', 'users-id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $json_data = array(
                'message' => "[ERROR]: ".validation_errors(),
                'data_name' => 'register_users',
                'num_data' => 0,
                'result' => 'failed',
            );
        }
        else
        {
            $users = $this->my_photo_model->get_users2(
                                                    $this->input->post('users-username', true), 
                                                    $this->input->post('users-password', true)
                                                );
            if (count($users) > 0){
                $json_data = array(
                    'message' => 'Users sudah ada. Tidak bisa ditambahkan',
                    'data_name' => 'register_users',
                    'num_data' => count($users),
                    'result' => 'failed',
                );

            }
            else if (count($users) == 0){
                $temp_data = array(
                    'users_username' => $this->input->post('users-username'),
                    'users_password' => $this->input->post('users-password'),
                    'users_email' => $this->input->post('users-email'),
                    'users_dob' => $this->input->post('users-dob'),
                    'users_biodata' => $this->input->post('users-biodata'),
                    'users_profile_photo' => $this->input->post('users-profile-photo'),
                    'users_rating' => $this->input->post('users-rating'),
                    'users_reg_id' => $this->input->post('users-reg-id'),
                    'users_phone_type' => $this->input->post('users-phone-type'),
                    'users_token' => $this->input->post('users-token'),
                    'users_username' => $this->input->post('users-username'),
                );
                
                $this->my_photo_model->edit_users($temp_data, $this->input->post('users-id'));
                $users = $this->my_photo_model->get_users($this->input->post('users-id'));
            
                $json_data = array(
                    'message' => 'Users berhasil diubah',
                    'data_name' => 'update_users',
                    'num_data' => count($users),
                    'result' => 'success',
                    'id_users' => $users->id_users,
                    'users_username' => $users->users_username,
                    'users_password' => $users->users_password,
                    'users_email' => $users->users_email,
                    'users_dob' => $users->users_dob,
                    'users_biodata' => $users->users_biodata,
                    'users_profile_photo' => $users->users_profile_photo,
                    'users_rating' => $users->users_rating,
                    'users_reg_id' => $users->users_reg_id,
                    'users_phone_type' => $users->users_phone_type,
                    'users_token' => $users->users_token,
                );
                
            }
            
            
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data,  JSON_PRETTY_PRINT);
    }
    
    function search()
    {
        $type = $this->input->post('users-search-type', true);
        $keyword = $this->input->post('users-search-keywords', true);
        $list_users = array();
        
        if ($type=='users-username')
        {
            $list_users = $this->my_photo_model->search_users('users_username', $keyword);
        }
        else if ($type=='users-facebook-email')
        {
            $list_users = $this->my_photo_model->search_users('users_facebook_email', $keyword);
        }
        else if ($type=='users-facebook-username')
        {
            $list_users = $this->my_photo_model->search_users('users_facebook_username', $keyword);
        }
    
        $json_data = array(
            'message' => 'Pencarian dengan keyword: '.$keyword.' dan jenis pencarian: '.$type.' berhasil dilakukan...',
            'data_name' => 'search_users',
            'num_data' => count($list_users),
            'result' => 'success',
            'data' => $list_users,
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data,  JSON_PRETTY_PRINT);
    }
    
    function login()
    {
       
       $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('users-email', 'users-email', 'required');
        $this->form_validation->set_rules('users-password', 'users-password', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $json_data = array(
                'message' => "Gagal Login",
                'data_name' => 'login_users',
                'num_data' => 0,
                'result' => 'failed',
            );
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $users = $this->my_photo_model->get_users_by_account($email, $password);
            $json_data = array(
                'message' => 'Login berhasil',
                'data_name' => 'login_users',
                'num_data' => count($users),
                'result' => 'success',
                'id_users' => $users->id_users,
                'users_username' => $users->users_username,
                'users_password' => $users->users_password,
                'users_email' => $users->users_email,
                'users_dob' => $users->users_dob,
                'users_biodata' => $users->users_biodata,
                'users_profile_photo' => $users->users_profile_photo,
                'users_rating' => $users->users_rating,
                'users_reg_id' => $users->users_reg_id,
                'users_phone_type' => $users->users_phone_type,
                'users_token' => $users->users_token,
            );            
            
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data,  JSON_PRETTY_PRINT);
    }

    function get_query(){
        $last_query = $this->my_photo_model->get_photo_by_id(1,1,3);
        echo $last_query;

        
    }

}

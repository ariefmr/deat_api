<html>
    <head>
        <title>Console Users</title>
        <link rel="stylesheet" type="text/css" />
        <script type="text/javascript" src=""></script>
    </head>
    <body>
        <h4>Input Photo</h4>
        <b>URL: <?php echo site_url('/api/my_photo/input')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/my_photo/input')?>" method="POST" target="_blank" enctype="multipart/form-data">
            <span style="color: red;">user photo name (*)</span>: <input type="text" name="user-photo-name" /> <br/><br/>
            <span style="color: red;">user photo description (*)</span>: <input type="text" name="user-photo-description" /> <br/><br/>
            <span style="color: red;">user photo upload date (*)</span>: <input type="text" name="user-photo-upload-date" /> <br/><br/>
            <span style="color: red;">user id (*)</span>: <input type="text" name="user-id" /> <br/><br/>
            <?php
            $attr_foto= array(      'name'  =>  "user-photo-path",
                                    'label' =>  $form['path_foto']['label'],
                                    'value' =>  kos($detail_bkp['path_foto'])
                                    );
            echo $this->mkform->input_image($attr_foto);
            ?>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        <br/><br/>
        
        <h4>Get Photo by User Id</h4>
        <b>URL: <?php echo site_url('/api/my_photo/get_photo_by_users_id')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/my_photo/get_photo_by_users_id')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">users_id (*)</span>: <input type="text" name="users-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

    </body>
</html>


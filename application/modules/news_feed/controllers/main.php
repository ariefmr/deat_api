<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
    {
        parent::__construct();
        $this->load->model('news_feed_model');
    }

	function index(){

        $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
        $add_css = array('select2.css', 'jquery.dataTables.css');

        $template = 'templates/page/v_form';
        $modules = 'news_feed';
        $views = 'news_feed_console';
        $labels = 'news_feed_label';

        $data['submit_form'] = '';

        echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}
    
    function get()
    {
        $temp_page = $this->input->post('page');
        $limit = $this->input->post('limit');
        $page = ($temp_page - 1) * $limit;
		$list_news_feed = $this->news_feed_model->get_all_news_feed($page, $limit);
        $json_data = array(
            'message' => 'Semua news_feed berhasil dimuat...',
            'data_name' => 'get_news_feed',
            'num_data' => count($list_news_feed),
            'result' => 'success',
            'data' => $list_news_feed,
            'news_feed_token' => ''
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function my_news_feed()
    {
       
       $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('users_id', 'users_id', 'required');
        $this->form_validation->set_rules('page', 'page', 'required');
        $this->form_validation->set_rules('limit', 'limit', 'required');
        
		if ($this->form_validation->run() == FALSE)
		{
			$json_data = array(
                'message' => "Gagal mengambil my news feed",
                'data_name' => 'my_news_feed',
                'num_data' => 0,
                'result' => 'failed',
            );
		}
		else
		{
            $temp_page = $this->input->post('page');
            $limit = $this->input->post('limit');
            $users_id = $this->input->post('users_id');
            $page = ($temp_page - 1) * $limit;
            $list_news_feed = $this->news_feed_model->get_my_news_feed($users_id, $page, $limit);
            $json_data = array(
                'message' => 'Semua news_feed berhasil dimuat...',
                'data_name' => 'my_news_feed',
                'num_data' => count($list_news_feed),
                'result' => 'success',
                'data' => $list_news_feed,
                'users_token' => ''
            );
		}
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data,  JSON_PRETTY_PRINT);
    }
    
    function my_bookmark_news_feed()
    {
       
       $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('users_id', 'users_id', 'required');
        $this->form_validation->set_rules('page', 'page', 'required');
        $this->form_validation->set_rules('limit', 'limit', 'required');
        
		if ($this->form_validation->run() == FALSE)
		{
			$json_data = array(
                'message' => "Gagal mengambil my news feed",
                'data_name' => 'my_news_feed',
                'num_data' => 0,
                'result' => 'failed',
            );
		}
		else
		{
            $temp_page = $this->input->post('page');
            $limit = $this->input->post('limit');
            $users_id = $this->input->post('users_id');
            $page = ($temp_page - 1) * $limit;
            $list_news_feed = $this->news_feed_model->get_my_bookmarked_feed($users_id, $page, $limit);
            $json_data = array(
                'message' => 'Semua news_feed berhasil dimuat...',
                'data_name' => 'my_news_feed',
                'num_data' => count($list_news_feed),
                'result' => 'success',
                'data' => $list_news_feed,
                'users_token' => ''
            );
		}
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data,  JSON_PRETTY_PRINT);
    }
    
    function register(){
        $this->load->library('form_validation');
        
        $json_data = array();
        
        
        $this->form_validation->set_rules('users-email', 'users-email', 'required');
        $this->form_validation->set_rules('users-username', 'users-username', 'required');
        $this->form_validation->set_rules('users-password', 'users-password', 'required');
            
		if ($this->form_validation->run() == FALSE)
		{
			$json_data = array(
                'message' => "[ERROR]: ".validation_errors(),
                'data_name' => 'register_users',
                'num_data' => 0,
                'result' => 'failed',
            );
		}
		else
		{
            $users = $this->users_model->get_users2(
                                                    $this->input->post('users-username', true), 
                                                    $this->input->post('users-password', true)
                                                );
            if (count($users) > 0){
                $json_data = array(
                    'message' => 'Users sudah ada. Tidak bisa ditambahkan',
                    'data_name' => 'register_users',
                    'num_data' => count($users),
                    'result' => 'failed',
                );

            }
            else if (count($users) == 0){
                $temp_data = array(
                    'users_username' => $this->input->post('users-username'),
                    'users_password' => $this->input->post('users-password'),
                    'users_email' => $this->input->post('users-email'),
                    'users_dob' => $this->input->post('users-dob'),
                    'users_gender' => $this->input->post('users-username'),
                );
                
                $users_id = $this->users_model->add_users($temp_data);
                $users = $this->users_model->get_users($users_id);
            
                $json_data = array(
                    'message' => 'Users berhasil terdaftar',
                    'data_name' => 'register_users',
                    'num_data' => count($users),
                    'result' => 'success',
                    'id_users' => $users->id_users,
                    'users_username' => $users->users_username,
                    'users_email' => $users->users_email,
                    'users_dob' => $users->users_dob,
                    'users_gender' => $users->users_gender,
                    'users_biodata' => $users->users_biodata,
                    'users_profile_photo' => $users->users_profile_photo,
                    'users_rating' => $users->users_rating,
                    'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
                );
                
            }
            
			
		}
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news_feed_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }
    
	public function get_all_news_feed($page, $limit)
    {
        $this->db->select('*');
        $this->db->order_by("news_feed_time", "desc");
        return $this->db->get('news_feed', $limit, $page)->result();
    }
    
    public function get_my_news_feed($users_id, $page, $limit)
    {
        $this->db->where(array('users_id'=>$users_id));
        $this->db->order_by("news_feed_time", "desc");
        return $this->db->get('news_feed', $limit, $page)->result();
    }
    
    function get_my_bookmarked_feed($users_id, $page, $limit)
    {
        $result = $this->db->query("SELECT *
                        FROM news_feed
                        WHERE users_id
                        IN (

                            SELECT bookmark_target_id
                            FROM bookmark b
                            WHERE b.users_id =".$users_id."
                        )
                        LIMIT ".$page." ,".$limit)->result();
        return $result;
        
        /*
         *  SELECT *
            FROM news_feed
            WHERE users_id
            IN (

                SELECT bookmark_target_id
                FROM bookmark b
                WHERE b.users_id =1
            )
            LIMIT 0 , 30
         */
    }
    
    public function get_news_feed($id)
    {
        $this->db->where(array('id_news_feed'=>$id));
        return $this->db->get('news_feed')->row();
    }
    
    public function get_news_feed2($name, $email){
        $this->db->where(array('news_feed_username'=>$name, 'news_feed_email'=>$email));
        return $this->db->get('news_feed')->row();
    }
    
    public function add_news_feed($data)
    {
        $this->db->insert('news_feed', $data);
        return $this->db->insert_id();
    }
    
    public function edit_news_feed($data, $id)
    {
        $this->db->where('id_news_feed', $id);
        $this->db->update('news_feed', $data);
    }
    
    public function search_news_feed($type, $keyword)
    {
        $this->db->like($type, $keyword, 'both');
        return $this->db->get('news_feed')->result();
    }
}

/* End of file band_model.php */
/* Location: ./application/controllers/band_model.php */

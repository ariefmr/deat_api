<html>
    <head>
        <title>Console news_feed</title>
        <link rel="stylesheet" type="text/css" />
        <script type="text/javascript" src=""></script>
    </head>
    <body>
        
        <h4>My news feed</h4>
        <b>URL: <?php echo site_url('/api/news_feed/my')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/news_feed/my')?>" method="POST" target="_blank">
            <span style="color: red;">users_token (*)</span>: <input type="text" name="users_token" /> <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <span style="color: red;">users_id (*)</span>: <input type="text" name="users_id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>

        
        <h4>Public news_feed</h4>
        <b>URL: <?php echo site_url('/api/news_feed/get')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/news_feed/get')?>" method="POST" target="_blank">
            <span style="color: red;">users_token (*)</span>: <input type="text" name="users_token" /> <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>
       
       <h4>My bookmark news feed</h4>
        <b>URL: <?php echo site_url('/api/news_feed/bookmark')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/news_feed/bookmark')?>" method="POST" target="_blank">
            <span style="color: red;">users_token (*)</span>: <input type="text" name="users_token" /> <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <span style="color: red;">users_id (*)</span>: <input type="text" name="users_id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
<!--    
        <h4>Add news_feed (not ready yet)</h4>
        <b>URL: <?php echo site_url('/api/news_feed/register')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/news_feed/register')?>" method="POST" target="_blank">
            <span style="font-color: red;"></span>users-token: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">news_feed-username (*)</span>: <input type="text" name="news_feed-username" /> <br/><br/>
            <span style="color: red;">news_feed-password (*)</span>: <input type="password" name="news_feed-password" /> <br/><br/>
            <span style="color: red;">news_feed-email (*)</span>: <input type="text" name="news_feed-email" /> <br/><br/>
            news_feed-dob: <input type="text" name="news_feed-dob" /> <br/><br/>
            news_feed-biodata: <br/><textarea rows="15" cols="100" name="news_feed-biodata"></textarea> <br/><br/>
            news_feed-profile-photo: <input type="text" name="news_feed-profile-photo" /> <br/><br/>
            news_feed-rating: <input type="text" name="news_feed-rating" /> <br/><br/>
            <span style="color: red;">news_feed-reg-id (*)</span>: <input type="text" name="news_feed-reg-id" /> <br/><br/>
            news_feed-phone-type: <input type="text" name="news_feed-phone-type" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

   
 
        <br/><br/>

        <h4>Update news_feed (not ready yet)</h4>
        <b>URL: <?php echo site_url('/api/news_feed/update')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/news_feed/update')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">news_feed-id (*)</span>: <input type="text" name="news_feed-id" /> <br/><br/>
            news_feed-username: <input type="text" name="news_feed-username" /> <br/><br/>
            news_feed-password: <input type="password" name="news_feed-password" /> <br/><br/>
            news_feed-email: <input type="text" name="news_feed-email" /> <br/><br/>
            news_feed-dob: <input type="text" name="news_feed-dob" /> <br/><br/>
            news_feed-biodata: <br/><textarea rows="15" cols="100" name="news_feed-biodata"></textarea> <br/><br/>
            news_feed-profile-photo: <input type="text" name="news_feed-profile-photo" /> <br/><br/>
            news_feed-rating: <input type="text" name="news_feed-rating" /> <br/><br/>
            news_feed-reg-id: <input type="text" name="news_feed-reg-id" /> <br/><br/>
            news_feed-phone-type: <input type="text" name="news_feed-phone-type" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>Search news_feed (not ready yet)</h4>
        <b>URL: <?php echo site_url('/api/news_feed/search')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/news_feed/search')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            news_feed-search-keywords: <input type="text" name="news_feed-search-keywords" /> <br/><br/>
            news_feed-search-type: <select name="news_feed-search-type">
                <option value="news_feed-username">news_feed-username</option>
                <option value="news_feed-facebook-email">news_feed-facebook-email</option>
                <option value="news_feed-facebook-username">news_feed-facebook-username</option>
            </select> <br/><br/>
            
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>Detail news_feed (not ready yet)</h4>
        <b>URL: <?php echo site_url('/api/news_feed/detail')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/news_feed/detail')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">news_feed-id (*)</span>: <input type="text" name="news_feed-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
    
        <br/><br/>
-->
        
    </body>
</html>


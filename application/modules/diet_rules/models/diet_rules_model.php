<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diet_rules_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }
    
	public function get_all_diet_rules($page, $limit)
    {
        $this->db->select('*');
        return $this->db->get('diet_rules', $limit, $page)->result();
    }
    
    // public function get_users_by_account($email, $password)
    // {
    //     $this->db->where(array('users_email'=>$email, 'users_password'=>$password));
    //     return $this->db->get('users')->row();
    // }
    public function get_diet_rules($id)
    {
        $this->db->where(array('id_diet_rules'=>$id));
        return $this->db->get('diet_rules')->row();
    }
    
     public function get_diet_rules_collection($page, $limit, $collection_id)
    {
        //$this->db->where(array('id_collection_rules'=>$id));
        $this->db->where(array('collection_rules_id'=>$collection_id));
        return $this->db->get('diet_rules', $limit, $page)->result();
    }

    // public function get_users2($name, $email){
    //     $this->db->where(array('users_username'=>$name, 'users_email'=>$email));
    //     return $this->db->get('users')->row();
    // }
    
    public function add_diet_rules($data)
    {
        $this->db->insert('diet_rules', $data);
        return $this->db->insert_id();
    }
    
    public function edit_diet_rules($data, $id)
    {
        $this->db->where('id_diet_rules', $id);
        $this->db->update('diet_rules', $data);
    }
    
    public function search_diet_rules($type, $keyword)
    {
        $this->db->like($type, $keyword, 'both');
        $result = $this->db->get('diet_rules');
        $q = $this->db->last_query();
        return $result->result();
    }

    public function get_all_collection_rules($page, $limit)
    {
        $this->db->select('*');
        return $this->db->get('collection_rules', $limit, $page)->result();
    }

    public function get_collection_rules($id)
    {
        $this->db->where(array('id_collection_rules'=>$id));
        return $this->db->get('collection_rules')->row();
    }

    public function get_collection_rules_users($id, $users_id)
    {
        //$this->db->where(array('id_collection_rules'=>$id));
        $this->db->where(array('id_collection_rules'=>$id, 'users_id'=>$users_id));
        return $this->db->get('collection_rules')->row();
    }
}

/* End of file band_model.php */
/* Location: ./application/controllers/band_model.php */

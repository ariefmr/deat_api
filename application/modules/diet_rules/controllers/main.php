<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
    {
        parent::__construct();
        $this->load->model('diet_rules_model');
    }

    function index(){
        // $this->load->view('my_photo_console');
        $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
        $add_css = array('select2.css', 'jquery.dataTables.css');

        $template = 'templates/page/v_form';
        $modules = 'diet_rules';
        $views = 'diet_rules_console';
        $labels = 'diet_rules_label';

        $data['submit_form'] = '';

        echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
    }
    
    function get()
    {
        $temp_page = $this->input->post('page');
        $limit = $this->input->post('limit');
        $page = ($temp_page - 1) * $limit;
		$list_diet_rules = $this->diet_rules_model->get_all_diet_rules($page, $limit);
        $json_data = array(
            'message' => 'Semua diet_rules berhasil dimuat...',
            'data_name' => 'get_diet_rules',
            'num_data' => count($list_diet_rules),
            'result' => 'success',
            'data' => $list_diet_rules,
            'users_token' => ''
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }

    function get_diet_collection()
    {
        $temp_page = $this->input->post('page');
        $limit = $this->input->post('limit');
        $page = ($temp_page - 1) * $limit;
        $collection_id = $this->input->post('collection_id');
        $list_diet_rules_collection = $this->diet_rules_model->get_diet_rules_collection($page, $limit, $collection_id);
        $json_data = array(
            'message' => 'Semua diet_rules_collection berhasil dimuat...',
            'data_name' => 'get_diet_rules_collection',
            'num_data' => count($list_diet_rules_collection),
            'result' => 'success',
            'data' => $list_diet_rules_collection,
            'users_token' => ''
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function getCollection()
    {
        $temp_page = $this->input->post('page');
        $limit = $this->input->post('limit');
        $page = ($temp_page - 1) * $limit;
        $list_collection_rules = $this->diet_rules_model->get_all_collection_rules($page, $limit);
        $json_data = array(
            'message' => 'Semua collection_rules berhasil dimuat...',
            'data_name' => 'get_collection_rules',
            'num_data' => count($list_collection_rules),
            'result' => 'success',
            'data' => $list_collection_rules,
            'users_token' => ''
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }

    function detail()
    {
        $json_data = array();
             $diet_rules = $this->diet_rules_model->get_diet_rules($this->input->post('diet_rules-id', true));
            //jika gagal
            if (count($diet_rules) == 0){
                $json_data = array(
                    'message' => 'Detail diet_rules gagal dimuat. diet_rules tidak ditemukan..',
                    'data_name' => 'get_diet_rules',
                    'num_data' => count($diet_rules),
                    'result' => 'failed',
                );

            }
            //jika berhasil
            else if (count($diet_rules) > 0){
                $json_data = array(
                    'message' => 'Detail diet_rules berhasil dimuat...',
                    'data_name' => 'get_diet_rules',
                    'num_data' => count($diet_rules),
                    'result' => 'success',
                    'id_diet_rules' => $diet_rules->id_diet_rules,
                    'diet_rules_title' => $diet_rules->diet_rules_title,
                    'diet_rules_description' => $diet_rules->diet_rules_description,
                    'diet_rules_type' => $diet_rules->diet_rules_type,
                    'diet_rules_time_from' => $diet_rules->diet_rules_time_from,
                    'diet_rules_time_to' => $diet_rules->diet_rules_time_to,
                    'icon_rules' => $diet_rules->icon_rules,
                    'users_id'=> $diet_rules->users_id,
                    'collection_rules_id' => $diet_rules->collection_rules_id,
                    'users_token' => '',
                );
            }
        // }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function input_rules()
    {
        $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('diet_rules-title', 'diet_rules-title', 'required');
        $this->form_validation->set_rules('diet_rules-type', 'diet_rules-type', 'required');
        //$this->form_validation->set_rules('diet_rules-time', 'diet_rules-time', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $json_data = array(
                'message' => "[ERROR]: ".validation_errors(),
                'data_name' => 'input_rules',
                'num_data' => 0,
                'result' => 'failed',
            );
        }
        else
        {
            
            $temp_data = array(
               
                'diet_rules_title' => $this->input->post('diet_rules-title'),
                'diet_rules_description' => $this->input->post('diet_rules-description'),
                'diet_rules_type' => $this->input->post('diet_rules-type'),
                'diet_rules_time_from' => $this->input->post('diet_rules-time_from'),
                'diet_rules_time_to' => $this->input->post('diet_rules-time_to'),
                'users_id'=> $this->input->post('users_id'),
                'collection_rules_id'=> $this->input->post('collection_rules_id')
            );
            
            $diet_rules_id = $this->diet_rules_model->add_diet_rules($temp_data);
            $diet_rules = $this->diet_rules_model->get_diet_rules($diet_rules_id);
        
            $json_data = array(
                'message' => 'Users berhasil terdaftar',
                'data_name' => 'register_rules',
                'num_data' => count($diet_rules),
                'result' => 'success',
                'id_diet_rules' => $diet_rules->id_diet_rules,
                'diet_rules_title' => $diet_rules->diet_rules_title,
                'diet_rules_type' => $diet_rules->diet_rules_type,
                'diet_rules_time_from' => $diet_rules->diet_rules_time_from,
                'diet_rules_time_to' => $diet_rules->diet_rules_time_to,
                'users_id' => $temp_data['users_id'],
                'collection_rules_id' => $temp_data['collection_rules_id']
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function update()
    {
    
        $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('diet_rules-id', 'diet_rules-id', 'required');
        
		if ($this->form_validation->run() == FALSE)
		{
			$json_data = array(
                'message' => "[ERROR]: ".validation_errors(),
                'data_name' => 'register_users',
                'num_data' => 0,
                'result' => 'failed',
            );
		}
		else
		{
            $temp_data = array(
           
                'diet_rules_title' => $this->input->post('diet_rules-title'),
                'diet_rules_description' => $this->input->post('diet_rules-description'),
                'diet_rules_type' => $this->input->post('diet_rules-type'),
                'diet_rules_time_from' => $this->input->post('diet_rules-time_from'),
                'diet_rules_time_to' => $this->input->post('diet_rules-time_to'),
                'users_id'=> $this->input->post('users_id'),
                'collection_rules_id'=> $this->input->post('collection_rules_id')
            );
        
            // $diet_rules_id = $this->diet_rules_model->add_diet_rules($temp_data);
            // $diet_rules = $this->diet_rules_model->get_diet_rules($diet_rules_id);
            $this->diet_rules_model->edit_diet_rules($temp_data, $this->input->post('diet_rules-id'));
            $diet_rules = $this->diet_rules_model->get_diet_rules($this->input->post('diet_rules-id'));
        
            $json_data = array(
                'message' => 'Diet rules berhasil diubah',
                'data_name' => 'update_rules',
                'num_data' => count($diet_rules),
                'result' => 'success',
                'id_diet_rules' => $diet_rules->id_diet_rules,
                'diet_rules_title' => $diet_rules->diet_rules_title,
                'diet_rules_type' => $diet_rules->diet_rules_type,
                'diet_rules_time_from' => $diet_rules->diet_rules_time_from,
                'diet_rules_time_to' => $diet_rules->diet_rules_time_to,
                'users_id'=> $temp_data['users_id'],
                'collection_rules_id'=> $temp_data['collection_rules_id']
            );
		}
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function search()
    {
        $type = $this->input->post('diet_rules-search-type', true);
        $keyword = $this->input->post('diet_rules-search-keywords', true);
        $list_diet_rules = array();
        $array_input = $this->input->post(NULL, True);
        // vdump($array_input);
        
        if ($array_input['diet_rules-search-type']=='1')
        {
            $list_diet_rules = $this->diet_rules_model->search_diet_rules('diet_rules_title', $keyword);
        }
        else if ($array_input['diet_rules-search-type']=='2')
        {
            $list_diet_rules = $this->diet_rules_model->search_diet_rules('diet_rules_title', $keyword);
        }
        else if ($array_input['diet_rules-search-type']=='3')
        {
            $list_diet_rules = $this->diet_rules_model->search_diet_rules('diet_rules_title', $keyword);
        }


        //  if ($type=='diet_rules-makanan')
        // {
        //     $list_diet_rules = $this->diet_rules_model->search_diet_rules('diet_rules_makanan', $keyword);
        // }
        // else if ($type=='diet_rules-minuman')
        // {
        //     $list_diet_rules = $this->diet_rules_model->search_diet_rules('diet_rules_minuman', $keyword);
        // }
        // else if ($type=='diet_rules-suplemen')
        // {
        //     $list_diet_rules = $this->diet_rules_model->search_diet_rules('diet_rules_type', $keyword);
        // }
    
        $json_data = array(
            'message' => 'Pencarian dengan keyword: '.$keyword.' dan jenis pencarian: '.$type.' berhasil dilakukan...',
            'data_name' => 'search_diet_rules',
            'num_data' => count($list_diet_rules),
            'result' => 'success',
            'data' => $list_diet_rules,
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
}

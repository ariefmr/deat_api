<html>
    <head>
        <title>Console Diet_rules</title>
        <link rel="stylesheet" type="text/css" />
        <script type="text/javascript" src=""></script>
    </head>
    <body>
    
        <h4>Get Diet_rules</h4>
        <b>URL: <?php echo site_url('/api/diet_rules/get')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/diet_rules/get')?>" method="POST" target="_blank">
            users-token : <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>

        <h4>Get Diet_rules_collection</h4>
        <b>URL: <?php echo site_url('/api/diet_rules/get_diet_collection')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/diet_rules/get_diet_collection')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <span style="color: red;">collection_id (*)</span>: <input type="text" name="collection_id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>
        
        <h4>Detail Diet_rules</h4>
        <b>URL: <?php echo site_url('/api/diet_rules/detail')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/diet_rules/detail')?>" method="POST" target="_blank">
            users-token : <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">diet_rules-id (*)</span>: <input type="text" name="diet_rules-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form> 
    
        <br/><br/>
        
        <h4>Input Diet_rules</h4>
        <b>URL: <?php echo site_url('/api/diet_rules/input_rules')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/diet_rules/input_rules')?>" method="POST" target="_blank">
            users-token : <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">diet_rules-title (*)</span>: <input type="text" name="diet_rules-title" /> <br/><br/>
            <span style="color: red;">diet_rules-description (*)</span> <br/> <textarea rows="15" cols="100" name="diet_rules-description"></textarea> <br/><br/>
            <span style="color: red;">diet_rules-type (*)</span>: <select name="diet_rules-type">
                <option value="1">makanan</option>
                <option value="2">minuman</option>
                <option value="3">suplemen</option>
            </select> <br/><br/>
            diet_rules-time_from : <input type="text" name="diet_rules-time_from" /> <br/><br/>
            diet_rules-time_to : <input type="text" name="diet_rules-time_to" /> <br/><br/>
            users_id : <input type="type" name="users_id" /> <br/><br/>
            collection_rules_id : <input type="type" name="collection_rules_id" /> <br/><br/>
    
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>

        <h4>Update Diet_rules</h4>
        <b>URL: <?php echo site_url('/api/diet_rules/update')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/diet_rules/update')?>" method="POST" target="_blank">
            users-token : <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">diet_rules-id (*)</span>: <input type="text" name="diet_rules-id" /> <br/><br/>
            <span style="color: red;">diet_rules-title (*)</span>: <input type="text" name="diet_rules-title" /> <br/><br/>
            diet_rules-description: <br/><textarea rows="15" cols="100" name="diet_rules-description"/></textarea> <br/><br/>
            <span style="color: red;">diet_rules-type (*)</span>: <select name="diet_rules-type">
                <option value="1">makanan</option>
                <option value="2">minuman</option>
                <option value="3">suplemen</option>
            </select> <br/><br/>
            diet_rules-time_from: <input type="text" name="diet_rules-time_from" /> <br/><br/>
            diet_rules-time_to: <input type="text" name="diet_rules-time_to" /> <br/><br/>
            users_id: <input type="type" name="users_id" /> <br/><br/>
            collection_rules_id : <input type="type" name="collection_rules_id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>Search Diet_rules</h4>
        <b>URL: <?php echo site_url('/api/diet_rules/search')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/diet_rules/search')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">diet_rules-search-keywords (*)</span>: <input type="text" name="diet_rules-search-keywords" /> <br/><br/>
            <span style="color: red;">diet_rules-search-type (*)</span>: <select name="diet_rules-search-type">
                <option value="1">makanan</option>
                <option value="2">minuman</option>
                <option value="3">suplemen</option>
            </select> <br/><br/>
            
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
    </body>
</html>


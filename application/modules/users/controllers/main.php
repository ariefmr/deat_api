<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
    }

	function index(){
        
        $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
        $add_css = array('select2.css', 'jquery.dataTables.css');

        $template = 'templates/page/v_form';
        $modules = 'users';
        $views = 'users_console';
        $labels = 'users_label';

        $data['submit_form'] = '';

        echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}
    
    function get(){
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
            $users_id = $this->input->post('users-id');
            $temp_page = $this->input->post('page');
            $limit = $this->input->post('limit');
            $page = ($temp_page - 1) * $limit;
            $list_users = $this->users_model->get_all_users($page, $limit, $users_id);
            
            if (count($list_users) == 0){
                $json_data = array(
                    'result' => 'no_data',
                    'message' => 'Semua users berhasil dimuat...',
                    'data_name' => 'get_users',
                    'num_data' => count($list_users),
                    'users_token' => $this->input->post('users-token'),
                );
            }
            else if (count($list_users) > 0){
                for ($i=0;$i < count($list_users); $i++){
                    $list_users[$i]->status_like = $this->users_model->is_liked_by_logged_in_users($users_id, $list_users[$i]->id_users);
                    $temp_users_age = (is_null($list_users[$i]->users_dob) ? "" : $this->age_generate($list_users[$i]->users_dob));
                    $list_users[$i]->users_age = $temp_users_age;
                    $list_users[$i]->collection_rules = $this->users_model->get_rules_by_users($list_users[$i]->id_users);
                    // pengecekan null contoh
                    $temp_users_phone_type = (is_null($list_users[$i]->users_phone_type) ? "" : $list_users[$i]->users_phone_type);
                    $list_users[$i]->users_phone_type = $temp_users_phone_type;
                    $temp_users_dob = (is_null($list_users[$i]->users_dob) ? "" : $list_users[$i]->users_dob);
                    $list_users[$i]->users_dob = $temp_users_dob;
                    $temp_users_biodata = (is_null($list_users[$i]->users_biodata) ? "" : $list_users[$i]->users_biodata);
                    $list_users[$i]->users_biodata = $temp_users_biodata;
                    $temp_users_profile_photo_id = (is_null($list_users[$i]->users_profile_photo_id) ? "" : $list_users[$i]->users_profile_photo_id);
                    $list_users[$i]->users_profile_photo_id = $temp_users_profile_photo_id;
                    $temp_users_background_photo = (is_null($list_users[$i]->users_background_photo) ? "" : $list_users[$i]->users_background_photo);
                    $list_users[$i]->users_background_photo = $temp_users_background_photo;
                    $temp_users_rating = (is_null($list_users[$i]->users_rating) ? "" : $list_users[$i]->users_rating);
                    $list_users[$i]->users_rating = $temp_users_rating;
                }
                $json_data = array(
                    'result' => 'success',
                    'message' => 'Semua users berhasil dimuat...',
                    'data_name' => 'get_users',
                    'num_data' => count($list_users),
                    'data' => $list_users,
                    'users_token' => $this->input->post('users-token'),
                );
            }
        
        } else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'detail_users',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function detail(){
        $json_data = array();
        
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
        
            if (is_null($this->input->post('users-id'))){
                $json_data = array(
                    'result' => 'failed',
                    'message' => 'Detail users gagal dimuat. ID Users tidak boleh kosong..',
                    'data_name' => 'get_users',
                    'num_data' => 0,
                );
            }
            else {
                $users = $this->users_model->get_users($this->input->post('users-id', true));
                if (count($users) == 0){
                    $json_data = array(
                        'result' => 'failed',
                        'message' => 'Detail users gagal dimuat. Users tidak ditemukan..',
                        'data_name' => 'detail_users',
                        'num_data' => count($users),
                    );

                }
                else if (count($users) > 0){
                    $json_data = array(
                        'result' => 'success',
                        'message' => 'Detail users berhasil dimuat...',
                        'data_name' => 'detail_users',
                        'num_data' => count($users),
                        'id_users' => $users->id_users,
                        'users_reg_id' => $users->users_reg_id,
                        'users_phone_type' => $users->users_phone_type,
                        'users_email' => $users->users_email,
                        'users_username' => $users->users_username,
                        'users_dob' => $users->users_dob,
                        'users_gender' => $users->users_gender,
                        'users_biodata' => $users->users_biodata,
                        'users_profile_photo_id' => $users->users_profile_photo_id,
                        'users_rating' => $users->users_rating,
                        'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
                    );
                }
            }
        } else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'detail_users',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function register(){
        $this->load->library('form_validation');
        
        $json_data = array();
        
        
        $this->form_validation->set_rules('users-email', 'users-email', 'required');
        $this->form_validation->set_rules('users-username', 'users-username', 'required');
        $this->form_validation->set_rules('users-password', 'users-password', 'required');
            
        if ($this->form_validation->run() == FALSE)
        {
            $json_data = array(
                'message' => "[ERROR]: ".validation_errors(),
                'data_name' => 'register_users',
                'num_data' => 0,
                'result' => 'failed',
            );
        }
        else
        {
            $users = $this->users_model->get_users2(
                                                    $this->input->post('users-username', true), 
                                                    $this->input->post('users-password', true)
                                                );
            if (count($users) > 0){
                $json_data = array(
                    'message' => 'Users sudah ada. Tidak bisa ditambahkan',
                    'data_name' => 'register_users',
                    'num_data' => count($users),
                    'result' => 'failed',
                );

            }
            else if (count($users) == 0){
                $temp_data = array(
                    'users_username' => $this->input->post('users-username'),
                    'users_password' => $this->input->post('users-password'),
                    'users_email' => $this->input->post('users-email'),
                    'users_dob' => $this->input->post('users-dob'),
                    'users_gender' => $this->input->post('users-gender'),
                );
                
                $users_id = $this->users_model->add_users($temp_data);
                $users = $this->users_model->get_users($users_id);
            
                $json_data = array(
                    'message' => 'Users berhasil terdaftar',
                    'data_name' => 'register_users',
                    'num_data' => count($users),
                    'result' => 'success',
                    'id_users' => $users->id_users,
                    'users_username' => $users->users_username,
                    'users_email' => $users->users_email,
                    'users_dob' => $users->users_dob,
                    'users_age' => $this->age_generate($users->users_dob),
                    'users_gender' => $users->users_gender,
                    'users_biodata' => $users->users_biodata,
                    'users_profile_photo_id' => $users->users_profile_photo_id,
                    'users_rating' => $users->users_rating,
                    'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
                );
                
            }
            
            
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function update(){
    
        $this->load->library('form_validation');
        
        $json_data = array();
        
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
        
        
            $this->form_validation->set_rules('users-id', 'users-id', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $json_data = array(
                    'message' => "[ERROR]: ".validation_errors(),
                    'data_name' => 'update_users',
                    'num_data' => 0,
                    'result' => 'failed',
                );
            }
            else
            {
                $users = $this->users_model->get_users2(
                                                        $this->input->post('users-username', true), 
                                                        $this->input->post('users-password', true)
                                                    );
                if (count($users) > 0){
                    $json_data = array(
                        'message' => 'Users sudah ada. Tidak bisa ditambahkan',
                        'data_name' => 'update_users',
                        'num_data' => count($users),
                        'result' => 'failed',
                    );

                }
                else if (count($users) == 0){
                    $temp_data = array(
                        'users_username' => $this->input->post('users-username'),
                        'users_email' => $this->input->post('users-email'),
                        'users_dob' => $this->input->post('users-dob'),
                        'users_biodata' => $this->input->post('users-biodata'),
                        'users_profile_photo_id' => $this->input->post('users-profile-photo'),
                        'users_rating' => $this->input->post('users-rating'),
                        'users_reg_id' => $this->input->post('users-reg-id'),
                        'users_phone_type' => $this->input->post('users-phone-type'),
                        'users_token' => $this->input->post('users-token'),
                        'users_username' => $this->input->post('users-username'),
                    );
                    
                    $this->users_model->edit_users($temp_data, $this->input->post('users-id'));
                    $users = $this->users_model->get_users($this->input->post('users-id'));
                
                    /*$json_data = array(
                        'message' => 'Users berhasil diubah',
                        'data_name' => 'update_users',
                        'num_data' => count($users),
                        'result' => 'success',
                        'id_users' => $users->id_users,
                        'users_username' => $users->users_username,
                        'users_email' => $users->users_email,
                        'users_dob' => $users->users_dob,
                        'users_biodata' => $users->users_biodata,
                        'users_profile_photo_id' => $users->users_profile_photo_id,
                        'users_rating' => $users->users_rating,
                        'users_reg_id' => $users->users_reg_id,
                        'users_phone_type' => $users->users_phone_type,
                        'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
                    );*/
                    // pengecekan null contoh
                    $temp_users_age = (is_null($users->users_dob) ? "" : $this->age_generate($users->users_dob));
                    $temp_users_phone_type = (is_null($users->users_phone_type) ? "" : $users->users_phone_type);
                    $temp_users_dob = (is_null($users->users_dob) ? "" : $users->users_dob);
                    $temp_users_biodata = (is_null($users->users_biodata) ? "" : $users->users_biodata);
                    $temp_users_profile_photo_id = (is_null($users->users_profile_photo_id) ? "" : $users->users_profile_photo_id);
                    $temp_users_background_photo = (is_null($users->users_background_photo) ? "" : $users->users_background_photo);
                    $temp_users_rating = (is_null($users->users_rating) ? "" : $users->users_rating);

                    $json_data = array(
                        'message' => 'Users berhasil diubah',
                        'data_name' => 'update_users',
                        'num_data' => count($users),
                        'result' => 'success',
                        'id_users' => $users->id_users,
                        'users_username' => $users->users_username,
                        'users_email' => $users->users_email,
                        'users_dob' => $temp_users_dob,
                        'users_age' => $temp_users_age,
                        'users_gender' => $users->users_gender,
                        'users_biodata' => $temp_users_biodata,
                        'users_phone_type' => $temp_users_phone_type,
                        'users_profile_photo_id' => $temp_users_profile_photo_id,
                        'users_background_photo' => $temp_users_background_photo,
                        'users_rating' => $temp_users_rating,
                        'users_reg_id' => $users->users_reg_id,
                        'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
                        'users_collection_rules' => $users->collection_rules_count,
                        'users_liked' => $users->liked_count,
                        'users_like' => $users->like_count,
                        'users_implement_by_other' => $users->implement_by_other_count,
                    );
                    
                }
                
                
            }
        
        } else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'update_users',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function search(){
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
        
        
            $type = $this->input->post('users-search-type', true);
            $keyword = $this->input->post('users-search-keywords', true);
            
            $temp_page = $this->input->post('page');
            $limit = $this->input->post('limit');
            $page = ($temp_page - 1) * $limit;
               
            $list_users = array();
            
            if ($type=='users-username')
            {
                $list_users = $this->users_model->search_users('users_username', $keyword, $page, $limit);
            }
            else if ($type=='users-email')
            {
                $list_users = $this->users_model->search_users('users_facebook_email', $keyword, $page, $limit);
            }
        
            $json_data = array(
                'message' => 'Pencarian dengan keyword: '.$keyword.' dan jenis pencarian: '.$type.' berhasil dilakukan...',
                'data_name' => 'search_users',
                'num_data' => count($list_users),
                'result' => 'success',
                'data' => $list_users,
                'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
            );
            
        }
        else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'search_users',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function login(){
       
       $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('users-email', 'users-email', 'required');
        $this->form_validation->set_rules('users-password', 'users-password', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $json_data = array(
                'message' => "Gagal Login",
                'data_name' => 'login_users',
                'num_data' => 0,
                'result' => 'failed',
            );
        }
        else
        {
            $email = $this->input->post('users-email');
            $password = $this->input->post('users-password');
        
            $temp_users = $this->users_model->get_users_by_account($email, $password);
            $users = $this->users_model->get_users($temp_users->id_users);
            $date = date('Y-m-d H:i:s');
            $users_token = md5($users->id_users."_".$date);
            $this->users_model->edit_users(array('users_reg_id' => $this->input->post('users-reg-id'), 'users_phone_type'=> $this->input->post('users-phone-type')), $users->id_users);
                    
            $this->users_model->set_users_token(array('users_token'=>$users_token), $users->id_users);

            // pengecekan null contoh
            $temp_users_age = (is_null($users->users_dob) ? "" : $this->age_generate($users->users_dob));
            $temp_users_phone_type = (is_null($users->users_phone_type) ? "" : $users->users_phone_type);
            $temp_users_dob = (is_null($users->users_dob) ? "" : $users->users_dob);
            $temp_users_biodata = (is_null($users->users_biodata) ? "" : $users->users_biodata);
            $temp_users_profile_photo_id = (is_null($users->users_profile_photo_id) ? "" : $users->users_profile_photo_id);
            $temp_users_background_photo = (is_null($users->users_background_photo) ? "" : $users->users_background_photo);
            $temp_users_rating = (is_null($users->users_rating) ? "" : $users->users_rating);

            $json_data = array(
                'message' => 'Login berhasil',
                'data_name' => 'login_users',
                'num_data' => count($users),
                'result' => 'success',
                'id_users' => $users->id_users,
                'users_username' => $users->users_username,
                'users_email' => $users->users_email,
                'users_dob' => $temp_users_dob,
                'users_age' => $temp_users_age,
                'users_gender' => $users->users_gender,
                'users_biodata' => $temp_users_biodata,
                'users_phone_type' => $temp_users_phone_type,
                'users_profile_photo_id' => $temp_users_profile_photo_id,
                'users_background_photo' => $temp_users_background_photo,
                'users_rating' => $temp_users_rating,
                'users_reg_id' => $users->users_reg_id,
                'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
                'users_collection_rules' => $users->collection_rules_count,
                'users_liked' => $users->liked_count,
                'users_like' => $users->like_count,
                'users_implement_by_other' => $users->implement_by_other_count,
            );
            
            
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function bookmark_profile(){
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
        
            $users_id = $this->input->post('users-id', true);
            $list_users = array();
            
            $list_users = $this->users_model->get_bookmarked_profile_by_users($users_id);
            
            $json_data = array(
                'message' => 'like profile ...',
                'data_name' => 'like_profile_users',
                'num_data' => count($list_users),
                'result' => 'success',
                'data' => $list_users,
            );
            
        }
        else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'like_profile',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function liker(){
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
        
            $users_id = $this->input->post('users-id', true);
            $list_users = array();
            
            $list_users = $this->users_model->get_users_liker($users_id);
            
            $json_data = array(
                'message' => 'liker users ...',
                'data_name' => 'liker_users',
                'num_data' => count($list_users),
                'result' => 'success',
                'data' => $list_users,
                'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
            );
            
        }
        else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'liker_users',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function implement_by_other(){
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
        
            $users_id = $this->input->post('users-id', true);
            $list_users = array();
            
            $list_users = $this->users_model->get_implemented_by_other($users_id);
            
            $json_data = array(
                'message' => 'implement by other ...',
                'data_name' => 'implemented_by_other',
                'num_data' => count($list_users),
                'result' => 'success',
                'data' => $list_users,
                'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
            );
            
        }
        else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'implemented_by_other',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function user_recommendation(){
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
        
            $users_id = $this->input->post('users-id', true);
            $list_users = array();
            
            $list_users = $this->users_model->get_recommendation_profile($users_id);
            
            $json_data = array(
                'message' => 'user_recommendation success ...',
                'data_name' => 'user_recommendation',
                'num_data' => count($list_users),
                'result' => 'success',
                'data' => $list_users,
                'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
            );
            
        }
        else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'user_recommendation',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function topten(){
        $auth = $this->users_model->auth_users($this->input->post('users-token'));
        
        if ($auth){
        
            $list_users = array();
            
            $list_users = $this->users_model->get_topten();
            
            $json_data = array(
                'message' => 'topten ...',
                'data_name' => 'topten',
                'num_data' => count($list_users),
                'result' => 'success',
                'data' => $list_users,
                'users_token' => 'c3fef41ee162b8134ca1a4f26571554b',
            );
            
        }
        else {
            $json_data = array(
                'result' => 'failed',
                'message' => 'You have now power...',
                'data_name' => 'topten',
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }

    function age_generate($date){
        //date in mm/dd/yyyy format; or it can be in other formats as well
        // $birthDate = "12/17/1983";
        $birthDate = $date;
        //explode the date to get month, day and year
        // $birthDate = explode("/", $birthDate);
        $birthDate = explode("-", $birthDate);
        $bulan = $birthDate[1];
        $tanggal = $birthDate[2];
        $tahun = $birthDate[0];
        
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $bulan, $tanggal, $tahun))) > date("md")
        ? ((date("Y") - $tahun) - 1)
        : (date("Y") - $tahun));
        
        // echo "Age is:" . $age;
        return $age;
    }

}

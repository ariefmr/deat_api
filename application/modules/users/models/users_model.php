<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {
    
    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }
    
    function get_all_users($page, $limit, $id)
    {
        $this->db->select('id_users, users_username, users_email, users_dob, users_gender, users_biodata, users_phone_type, users_profile_photo_id, users_background_photo, users_rating,  (SELECT count(*) FROM `bookmark` WHERE 
                                bookmark_target_id = users.id_users and bookmark_implement != "" and bookmark_implement != 0) as implement_by_other_count');
        $this->db->where('id_users !=', $id);
        
        return $this->db->get('users', $limit, $page)->result();
    }
    
    function get_users_by_account($email, $password)
    {
        
        $this->db->where(array('users_email'=>$email, 'users_password'=>$password));
        return $this->db->get('users')->row();
    }
    
    function get_users($id)
    {
        $this->db->where(array('id_users'=>$id));
        $this->db->select('id_users, users_username, users_email, users_dob, users_gender, (SELECT count(*)
                            FROM collection_rules
                            WHERE users_id ='.$id.') as collection_rules_count, (SELECT count(*)
                            FROM bookmark
                            WHERE users_id ='.$id.') as like_count, (SELECT count(*)
                            FROM bookmark
                            WHERE bookmark_target_id ='.$id.') as liked_count, (SELECT count(*) FROM `bookmark` WHERE 
                            bookmark_target_id = '.$id.' and bookmark_implement != "" and bookmark_implement != 0) as implement_by_other_count,
                            users_biodata, users_profile_photo_id, users_background_photo, users_rating, users_reg_id, users_phone_type, users_token');
        return $this->db->get('users')->row();
    }
    
    function get_users2($name, $email){
        $this->db->where(array('users_username'=>$name, 'users_email'=>$email));
        return $this->db->get('users')->row();
    }
    
    function add_users($data)
    {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }
    
    function edit_users($data, $id)
    {
        $this->db->where('id_users', $id);
        $this->db->update('users', $data);
    }
    
    function search_users($type, $keyword, $page, $limit)
    {
        $this->db->like($type, $keyword, 'both');
        return $this->db->get('users', $limit, $page)->result();
    }
    
    function set_users_token($data, $id)
    {
        $this->db->where('id_users', $id);
        $this->db->update('users', $data);
    }
    
    function auth_users($users_token){
        //~ $this->db->where(array('users_token'=>$users_token));
        //~ $row = $this->db->get('users')->result();
        //~ if (count($row) == 1){
            //~ return true;
        //~ } else {
            //~ return false;
        //~ }
        
        if ($users_token == 'c3fef41ee162b8134ca1a4f26571554b'){
            return true;
        } else {
            return false;
        }
    }
    
    function get_rules_by_users($id){
        $this->db->where('users_id', $id);
        return $this->db->get('collection_rules')->result();
    }
    
    function get_users_by_token($token){
        $this->db->where('users_token', $token);
        $this->db->select('id_users');
        $row = $this->db->get('users')->row();
        return $row->id_users;
    }
    
    function is_liked_by_logged_in_users($users_id, $id){
        $this->db->where(array('users_id'=>$users_id, 'bookmark_target_id'=>$id));
        $row = $this->db->get('bookmark')->row();
        if (count($row) == 1){
            return true;
        } else {
            return false;
        }
    }
    
    function get_bookmarked_profile_by_users($users_id){
        $this->db->select('id_users, users_username, users_email, users_dob, users_gender, users_biodata, users_profile_photo_id, users_background_photo, users_rating,  (SELECT count(*) FROM `bookmark` WHERE 
                                bookmark_target_id = users.id_users and bookmark_implement != "" and bookmark_implement != 0) as implement_by_other_count');
        $this->db->where('id_users !=', $users_id);
        $this->db->where('id_users in (SELECT bookmark_target_id
                            FROM bookmark
                            WHERE users_id ='.$users_id.')');
        
        return $this->db->get('users')->result();
    }
    
    function get_users_liker($users_id){
        $this->db->select('id_users, users_username, users_email, users_dob, users_gender, users_biodata, users_profile_photo_id, users_background_photo, users_rating,  (SELECT count(*) FROM `bookmark` WHERE 
                                bookmark_target_id = users.id_users and bookmark_implement != "" and bookmark_implement != 0) as implement_by_other_count');
        $this->db->where('id_users !=', $users_id);
        $this->db->where('id_users in (SELECT users_id
                            FROM bookmark
                            WHERE bookmark_target_id ='.$users_id.')');
        
        return $this->db->get('users')->result();
    }
    
    function get_implemented_by_other($users_id){
        $query = $this->db->query("SELECT cr.collection_rules_name, cr.collection_rules_description, 
                                        usr.users_username, usr.users_email, usr.users_dob, usr.users_gender, usr.users_biodata, usr.users_profile_photo_id, usr.users_background_photo 
                        FROM ( 
                            SELECT * 
                            FROM bookmark 
                            WHERE bookmark_target_id = ".$users_id." 
                            AND bookmark_implement != '' 
                            AND bookmark_implement !=0
                        ) im, collection_rules cr, users usr
                        WHERE im.bookmark_implement = cr.id_collection_rules and 
                        im.users_id=usr.id_users ");
        
        return $query->result();
    }
    
    function get_recommendation_profile($users_id){
        $query = $this->db->query('select * from users where id_users in 
                                        (select bookmark_target_id from bookmark where users_id in 
                                                (SELECT bookmark_target_id FROM bookmark WHERE users_id = '.$users_id.') 
                                                and bookmark_target_id != '.$users_id.') limit 0, 6');
        return $query->result();
    }
    
    function get_topten(){
        $this->db->order_by('users_rating', 'desc');
        $this->db->select('id_users, users_username, users_email, users_dob, users_gender, users_biodata, users_profile_photo_id, users_background_photo, users_rating,  (SELECT count(*) FROM `bookmark` WHERE 
                                bookmark_target_id = users.id_users and bookmark_implement != "" and bookmark_implement != 0) as implement_by_other_count');
        return $this->db->get('users', '10', '0')->result();
    }
}

/* End of file band_model.php */
/* Location: ./application/controllers/band_model.php */

<html>
    <head>
        <title>Console Users</title>
        <link rel="stylesheet" type="text/css" />
        <script type="text/javascript" src=""></script>
    </head>
    <body>
        <h4>Get Users</h4>
        <b>URL: <?php echo site_url('/api/users/get')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/users/get')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">users-id (*)</span>: <input type="text" name="users-id" /> <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>
        
        <h4>Detail Users</h4>
        <b>URL: <?php echo site_url('/api/users/detail')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/users/detail')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">users-id (*)</span>: <input type="text" name="users-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
    
        <br/><br/>
        
        <h4>Register Users</h4>
        <b>URL: <?php echo site_url('/api/users/register')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/users/register')?>" method="POST" target="_blank">
            <span style="color: red;">users-name (*)</span>: <input type="text" name="users-username" /> <br/><br/>
            <span style="color: red;">users-password (*)</span>: <input type="password" name="users-password" /> <br/><br/>
            <span style="color: red;">users-email (*)</span>: <input type="text" name="users-email" /> <br/><br/>
            users-dob: <input type="text" name="users-dob" /> <br/><br/>
            users-gender: <select name="users-gender">
                <option value="male">male</option>
                <option value="female">female</option>
            </select> <br/><br/>
<!--
            users-biodata: <br/><textarea rows="15" cols="100" name="users-biodata"></textarea> <br/><br/>
-->
<!--
            users-profile-photo: <input type="text" name="users-profile-photo" /> <br/><br/>
-->
<!--
            users-rating: <input type="text" name="users-rating" /> <br/><br/>
-->
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>

        <h4>Update Users</h4>
        <b>URL: <?php echo site_url('/api/users/update')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/users/update')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">users-id (*)</span>: <input type="text" name="users-id" /> <br/><br/>
            users-username: <input type="text" name="users-username" /> <br/><br/>
            users-password: <input type="password" name="users-password" /> <br/><br/>
            users-email: <input type="text" name="users-email" /> <br/><br/>
            users-dob: <input type="text" name="users-dob" /> <br/><br/>
            users-gender: <select name="users-gender">
                <option value="male">male</option>
                <option value="female">female</option>
            </select> <br/><br/>
            users-biodata: <br/><textarea rows="15" cols="100" name="users-biodata"></textarea> <br/><br/>
<!--
            users-profile-photo: <input type="text" name="users-profile-photo" /> <br/><br/>
-->
<!--
            users-rating: <input type="text" name="users-rating" /> <br/><br/>
-->
<!--
            users-reg-id: <input type="text" name="users-reg-id" /> <br/><br/>
-->
<!--
            users-phone-type: <input type="text" name="users-phone-type" /> <br/><br/>
-->
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>Search Users</h4>
        <b>URL: <?php echo site_url('/api/users/search')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/users/search')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            users-search-keywords: <input type="text" name="users-search-keywords" /> <br/><br/>
            users-search-type: <select name="users-search-type">
                <option value="users-username">users-username</option>
                <option value="users-email">users-email</option>
            </select>
            <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <br/><br/>
            
            
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>Login Users</h4>
        <b>URL: <?php echo site_url('/api/users/login')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/users/login')?>" method="POST" target="_blank">
            <span style="color: red;">users-email (*)</span>: <input type="text" name="users-email" /> <br/><br/>
            <span style="color: red;">users-password (*)</span>: <input type="text" name="users-password" /> <br/><br/>
            users-reg-id: <input type="text" name="users-reg-id" /> <br/><br/>
            users-phone-type: <input type="text" name="users-phone-type" /> <br/><br/>
            
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>Profile Liked by Users</h4>
        <b>URL: <?php echo site_url('/api/users/like_profile')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/users/like_profile')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            users-id: <input type="text" name="users-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>Liker Users</h4>
        <b>URL: <?php echo site_url('/api/users/liker')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/users/liker')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            users-id: <input type="text" name="users-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <h4>Our Rule Users</h4>
        <b>URL: <?php echo site_url('/api/users/rule_users')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/users/rule_users')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            users-id: <input type="text" name="users-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>User Recommendation</h4>
        <b>URL: <?php echo site_url('/api/users/user_recommendation')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/users/user_recommendation')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            users-id: <input type="text" name="users-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
        
        <h4>Top ten</h4>
        <b>URL: <?php echo site_url('/api/users/topten')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/users/topten')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>
    </body>
</html>


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dev extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
	}
	
	public function index()
	{
		$this->load->config('labels');

		$data['database_timeline'] = $this->config->item('database_timeline');

		$data['labels'] =  Array(
									'form_contoh' => $this->config->item('form_contoh')
								);
		echo Modules::run('templates/page/v_form', //tipe template
							'dev', //nama module
							'index', //nama file view
							'', //dari labels.php
							'', //plugin javascript khusus untuk page yang akan di load
							'', //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
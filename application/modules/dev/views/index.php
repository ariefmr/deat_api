<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>List Updated Database!</h3>
			</div>
			<div class="panel-body">
				<p>History update database<br> <strong>(file: application/config/db_timeline.php)</strong> :</p>
				<?php foreach ($database_timeline as $item): ?>
					<div class="panel">
						<strong title="tanggal_update">( <?php echo $item['tanggal_update']; ?> )</strong>
						Link: <a href="<?php echo $item['link_dropbox']; ?>">
						<?php echo $item['nama_file_dropbox']; ?>
						</a>
						<br>
						Notes:
						<?php echo $item['notes']; ?>
						(<em title="pelaku"><?php echo $item['pelaku']; ?></em>)
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Daftar Custom Constants!</h3>
			</div>
			<div class="panel-body">
				<div class="panel-group" id="accordion">
					<?php foreach ($labels as $label_name => $configs): ?>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						    	<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $label_name ?>">
						      <h4 class="panel-title">
						       
						          <?php echo "<h4>CONFIG: ".$label_name."</h4><br>" ?>
						        
						      </h4>
						      	</a>
						    </div>
						    <div id="<?php echo $label_name ?>" class="panel-collapse collapse">
						      <div class="panel-body">
						      	<?php if (!empty($configs)): ?>
						      		<?php foreach ($configs as $index => $lists): ?>
										<?php echo "<h4>INDEX: ".$index."</h4><br>" ?>
												<?php if (!empty($lists)): ?>
													<?php foreach ($lists as $sub_index => $values): ?>
														<?php 
															if(is_array($values))
															{
																echo "<p>";
																echo "<strong>tag: ".$sub_index."</strong>";
																echo "<br>";
																foreach ($values as $key => $value) {
																	echo $key.": ".$value;
																	echo "<br>";
																}
																echo "</p>";
															}else{
																echo $sub_index.": ".$values;
															}
														?>
													<br>
													<?php endforeach ?>
												<?php endif ?>
									<?php endforeach ?>
						      	<?php endif ?>
								</div>
							</div>
						</div>
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>

</div>

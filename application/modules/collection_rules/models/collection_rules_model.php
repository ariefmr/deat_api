<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Collection_rules_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }
    
    public function get_all_collection_rules($page, $limit)
    {
        $this->db->select('*');
        return $this->db->get('collection_rules', $limit, $page)->result();
    }

    public function get_collection_rules($id)
    {
        $this->db->where(array('id_collection_rules'=>$id));
        return $this->db->get('collection_rules')->row();
    }

    public function get_collection_rules_users($page, $limit, $users_id)
    {
        //$this->db->where(array('id_collection_rules'=>$id));
        $this->db->where(array('users_id'=>$users_id));
        return $this->db->get('collection_rules', $limit, $page)->result();
    }

    public function add_collection_rules($data)
    {
        $this->db->insert('collection_rules', $data);
        return $this->db->insert_id();
    }

     public function edit_collection_rules($data, $id)
    {
        $this->db->where('id_collection_rules', $id);
        $this->db->update('collection_rules', $data);
    }

    public function search_collection_rules($type, $keyword)
    {
        $this->db->like($type, $keyword, 'both');
        $result = $this->db->get('collection_rules');
        $q = $this->db->last_query();
        return $result->result();
    }
}

/* End of file band_model.php */
/* Location: ./application/controllers/band_model.php */

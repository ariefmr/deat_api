<div class="row">
  <div class="col-lg-12">
          <?php
          // vdump($detail_pendok, false);
          $hidden_input = array('id_pendok' => '',
                                'submit_to' => 'edit');
          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);
         
 
          $attr_nama_kapal = array( 'name' => 'test',
                                    'label' => 'test',
                                    'value' => '',
                                    'disabled' => ''
                    );
          echo $this->mkform->input_text($attr_nama_kapal);  

          $attr_no_permohonan = array( 'name' => 'test',
                                        'label' => 'test',
                                        'value' => '',
                                        'disabled' => FALSE
                    );
          echo $this->mkform->input_text($attr_no_permohonan);

          $attr_tanggal_surat_permohonan = array( 
                           'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
                          // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
                          'default_date' => '', // opsi: '', tidak wajib ada
                          'placeholder' => '', // wajib ada atau '' ('')
                          'name' => 'date_test',
                          'label' => 'date_test'
                        );
          echo $this->mkform->input_date($attr_tanggal_surat_permohonan);

          $attr_tipe_permohonan = array( 'name' => 'test',
                                        'label' => 'test',
                                        'opsi' => array('BARU' => 'BARU', 'PERUBAHAN' => 'PERUBAHAN'),
                                        'value' => ''
                    );
          echo $this->mkform->input_select($attr_tipe_permohonan); 
          ?>
          
          <div class="col-md-6">
            <div id="area_cari_pemohon" style="margin-bottom:15px; display:block;">
                      <?php 
                      $nama_pemohon = '';
                      $id_pemohon   = '';
                      echo Modules::run('refkapi/mst_pemohon/pilih_pemohon',$nama_pemohon,$id_pemohon);
                      ?> 
            </div>           
          </div>

          <div class="col-md-6">
                <div id="area_cari_perusahaan" style="margin-bottom:15px; display:block;">
                  <?php 
                  $np = '';
                  $ip   = '';
                  $npj   = '';
                  $nipj   = '';
                  $ttl   = '';
                  $ap   = '';
                  echo Modules::run('refdss/mst_perusahaan/pilih_perusahaan',$np,$ip,$npj,$nipj,$ttl,$ap);
                  ?> 
                </div>
          </div>

          <?php
          $attr_keterangan_pendok = array('name' => 'test',
                                          'label' => 'test',
                                          'value' => '',
                                          'rows' => '3'
                    );
          echo $this->mkform->input_textarea($attr_keterangan_pendok);


         ?>
  </div>
</div>

       
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="submit" class="btn btn-primary btn-submit" data-submit-to="edit">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>
<script>
  var is_final = "<?php echo $detail_pendok['status_pendok'] ?>";


  var submit_listener = function(){
    $(".btn-submit").click(function(event){
      event.preventDefault();
      var submit_to = $(this).data('submitTo');
      $("input[name=submit_to]").val(submit_to);
      $("#form_entry").submit();
    });
  };
  s_func.push(submit_listener);
  // s_func.push(cek_final);
</script>

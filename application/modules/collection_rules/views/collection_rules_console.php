<html>
    <head>
        <title>Console Diet_rules</title>
        <link rel="stylesheet" type="text/css" />
        <script type="text/javascript" src=""></script>
    </head>
    <body>
    
        <h4>Get Collection_rules</h4>
        <b>URL: <?php echo site_url('/api/collection_rules/get')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/collection_rules/get')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>

        <h4>Get Collection_rules_users</h4>
        <b>URL: <?php echo site_url('/api/collection_rules/get_collection_users')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/collection_rules/get_collection_users')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">page (*)</span>: <input type="text" name="page" /> <br/><br/>
            <span style="color: red;">limit (*)</span>: <input type="text" name="limit" /> <br/><br/>
            <span style="color: red;">users_id (*)</span>: <input type="text" name="users_id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>

        <h4>Detail collection_rules</h4>
        <b>URL: <?php echo site_url('/api/collection_rules/detail')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/collection_rules/detail')?>" method="POST" target="_blank">
            <span style="color: red;">users-token (*)</span>: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">collection_rules-id (*)</span>: <input type="text" name="collection_rules-id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form> 
        
        <br/><br/>

        <h4>Input Collection_rules</h4>
        <b>URL: <?php echo site_url('/api/collection_rules/input_collection')?></b> <br/> <br/>
        <form action="<?php echo site_url('/api/collection_rules/input_collection')?>" method="POST" target="_blank">
            <span style="font-color: red;"></span>users-token: <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">collection_rules-name (*)</span>: <input type="text" name="collection_rules-name" /> <br/><br/>
            collection_rules-description: <br/><textarea rows="15" cols="100" name="collection_rules-description"></textarea> <br/><br/>
            users_id: <input type="type" name="users_id" /> <br/><br/>
    
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>

        <br/><br/>

        <h4>Update Collection_rules</h4>
        <b>URL: <?php echo site_url('/api/collection_rules/update')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/collection_rules/update')?>" method="POST" target="_blank">
            users-token : <input type="text" name="users-token" /> <br/><br/>
            <span style="color: red;">Collection_rules-id (*)</span>: <input type="text" name="collection_rules-id" /> <br/><br/>
            <span style="color: red;">Collection_rules-name (*)</span>: <input type="text" name="collection_rules-name" /> <br/><br/>
            Collection_rules-description: <br/><textarea rows="15" cols="100" name="collection_rules-description"/></textarea> <br/><br/>
            users_id: <input type="type" name="users_id" /> <br/><br/>
            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>

        <h4>Search Diet_rules</h4>
        <b>URL: <?php echo site_url('/api/collection_rules/search')?></b> <br/><br/>
        <form action="<?php echo site_url('/api/collection_rules/search')?>" method="POST" target="_blank">
            users-token: <input type="text" name="users-token" /> <br/><br/>
            collection_rules-search-keywords: <input type="text" name="collection_rules-search-keywords" /> <br/><br/>

            <input type="submit" value="Submit" />
            <input type="reset" value="Reset" />
        </form>
        
        <br/><br/>


    </body>
</html>


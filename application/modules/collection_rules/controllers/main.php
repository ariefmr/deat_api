<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
    {
        parent::__construct();
        $this->load->model('collection_rules_model');
    }

    function index(){
        $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
        $add_css = array('select2.css', 'jquery.dataTables.css');

        $template = 'templates/page/v_form';
        $modules = 'collection_rules';
        $views = 'collection_rules_console';
        $labels = 'collection_rules_label';

        $data['submit_form'] = '';

        echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
    }
    
    function get()
    {
        $temp_page = $this->input->post('page');
        $limit = $this->input->post('limit');
        $page = ($temp_page - 1) * $limit;
		$list_collection_rules = $this->collection_rules_model->get_all_collection_rules($page, $limit);
        $json_data = array(
            'message' => 'Semua collection_rules berhasil dimuat...',
            'data_name' => 'get_collection_rules',
            'num_data' => count($list_collection_rules),
            'result' => 'success',
            'data' => $list_collection_rules,
            'users_token' => ''
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }

    function get_collection_users()
    {
        $temp_page = $this->input->post('page');
        $limit = $this->input->post('limit');
        $page = ($temp_page - 1) * $limit;
        $users_id = $this->input->post('users_id');
        $list_collection_rules_users = $this->collection_rules_model->get_collection_rules_users($page, $limit, $users_id);
        $json_data = array(
            'message' => 'Semua collection_rules_users berhasil dimuat...',
            'data_name' => 'get_collection_rules_users',
            'num_data' => count($list_collection_rules_users),
            'result' => 'success',
            'data' => $list_collection_rules_users,
            'users_token' => ''
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }

    function detail()
    {
        $json_data = array();
        
            //jika gagal
            $collection_rules = $this->collection_rules_model->get_collection_rules($this->input->post('collection_rules-id', true));
            if (count($collection_rules) == 0){
                $json_data = array(
                    'message' => 'Detail collection_rules gagal dimuat. collection_rules tidak ditemukan..',
                    'data_name' => 'get_collection_rules',
                    'num_data' => count($collection_rules),
                    'result' => 'failed',
                );

            }
            //jika berhasil
            else if (count($collection_rules) > 0){
                $json_data = array(
                    'message' => 'Detail collection_rules berhasil dimuat...',
                    'data_name' => 'get_collection_rules',
                    'num_data' => count($collection_rules),
                    'result' => 'success',
                    'id_collection_rules' => $collection_rules->id_collection_rules,
                    'collection_rules_name' => $collection_rules->collection_rules_name,
                    'collection_rules_description' => $collection_rules->collection_rules_description,
                    'collection_rules_status_publish' => $collection_rules->collection_rules_status_publish,
                    'users_id'=> $collection_rules->users_id,
                    'users_token' => '',
                );
            }
        // }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
    function input_collection()
    {
        $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('collection_rules-name', 'collection_rules-name', 'required');
        //$this->form_validation->set_rules('diet_rules-type', 'diet_rules-type', 'required');
        //$this->form_validation->set_rules('diet_rules-time', 'diet_rules-time', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $json_data = array(
                'message' => "[ERROR]: ".validation_errors(),
                'data_name' => 'input_rules',
                'num_data' => 0,
                'result' => 'failed',
            );
        }
        else
        {
            
            $temp_data = array(
               
                'collection_rules_name' => $this->input->post('collection_rules-name'),
                // 'collection_rules_description' => $this->input->post('collection_rules-description'),
                // 'collection_rules_type' => $this->input->post('collection_rules-type'),
                // 'collection_rules_time' => $this->input->post('collection_rules-time'),
                'users_id'=> $this->input->post('users_id')
            );
            
            $collection_rules_id = $this->collection_rules_model->add_collection_rules($temp_data);
            $collection_rules = $this->collection_rules_model->get_collection_rules($collection_rules_id);
        
            $json_data = array(
                'message' => 'collection_rules berhasil diinput',
                'data_name' => 'input_collection',
                'num_data' => count($collection_rules),
                'result' => 'success',
                'id_collection_rules' => $collection_rules->id_collection_rules,
                'collection_rules_name' => $collection_rules->collection_rules_name,
                'collection_rules_description' => $collection_rules->collection_rules_description,
                'users_id' => $temp_data['users_id']
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }

    function update()
    {
    
        $this->load->library('form_validation');
        
        $json_data = array();
        
        $this->form_validation->set_rules('collection_rules-id', 'collection_rules-id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $json_data = array(
                'message' => "[ERROR]: ".validation_errors(),
                'data_name' => 'register_users',
                'num_data' => 0,
                'result' => 'failed',
            );
        }
        else
        {
            $temp_data = array(
           
                'collection_rules_name' => $this->input->post('collection_rules-name'),
                'collection_rules_description' => $this->input->post('collection_rules-description'),
                'users_id'=> $this->input->post('users_id')
            );
        
            // $diet_rules_id = $this->diet_rules_model->add_diet_rules($temp_data);
            // $diet_rules = $this->diet_rules_model->get_diet_rules($diet_rules_id);
            $this->collection_rules_model->edit_collection_rules($temp_data, $this->input->post('collection_rules-id'));
            $collection_rules = $this->collection_rules_model->get_collection_rules($this->input->post('collection_rules-id'));
        
            $json_data = array(
                'message' => 'Collection rules berhasil diubah',
                'data_name' => 'update_collection',
                'num_data' => count($collection_rules),
                'result' => 'success',
                'id_collection_rules' => $collection_rules->id_collection_rules,
                'collection_rules_name' => $collection_rules->collection_rules_name,
                'collection_rules_description' => $collection_rules->collection_rules_description,
                'users_id'=> $temp_data['users_id']
            );
        }
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }

    function search()
    {
        //$type = $this->input->post('collection_rules-search-type', true);
        $keyword = $this->input->post('collection_rules-search-keywords', true);
        $list_collection_rules = array();
        $array_input = $this->input->post(NULL, True);
        // vdump($array_input);

        $list_collection_rules = $this->collection_rules_model->search_collection_rules('collection_rules_name', $keyword);
        

        $json_data = array(
            'message' => 'Pencarian dengan keyword: '.$keyword,
            'data_name' => 'search_collection_rules',
            'num_data' => count($list_collection_rules),
            'result' => 'success',
            'data' => $list_collection_rules,
        );
        
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header('HTTP/1.1 404 Not Found');
        echo json_encode($json_data);
    }
    
}

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
* 
| ---------------------------------------------------------------------------
* Pada intinya, sebuah API terdiri dari CRUDS (Create, Read, Update, Delete, Search)
* Untuk kebutuhan API aplikasi mobile yang dikembangkan rizky. Ada beberapa hal yang
* harus diperhatikan. Pertama URL harus seperti berikut base_url/api/fitur/fungsional
* contoh: http://deat.devnila.com/api/users/register
* 
* semua API harus disimpan dalam bentuk POST. 
* 
* Beberapa hal yang perlu diperhatikan:
* get = mengambil semua isi tabel
* detail = mengambil salah satu baris di tabel
* search = mengambil data tabel berdasarkan kriteria tertentu
* edit = mengubah salah satu baris di tabel
* delete = menghapus salah satu baris di tabel
* add = menambah data baru ke tabel
*/

$route['default_controller'] = "dev";
$route['404_override'] = '';

// route untuk fitur users
$route['api/console/users'] = 'users/main';
$route['api/users/get'] = 'users/main/get';
$route['api/users/detail'] = 'users/main/detail';
$route['api/users/register'] = 'users/main/register';
$route['api/users/update'] = 'users/main/update';
$route['api/users/login'] = 'users/main/login';
$route['api/users/search'] = 'users/main/search';
$route['api/users/like_profile'] = 'users/main/bookmark_profile';
$route['api/users/liker'] = 'users/main/liker';
$route['api/users/rule_users'] = 'users/main/implement_by_other';
$route['api/users/user_recommendation'] = 'users/main/user_recommendation';
$route['api/users/topten'] = 'users/main/topten';
$route['api/users/age_generate'] = 'users/main/age_generate';

// route untuk fitur news_feed
$route['api/console/news_feed'] = 'news_feed/main';
$route['api/news_feed/get'] = 'news_feed/main/get';
$route['api/news_feed/my'] = 'news_feed/main/my_news_feed';
$route['api/news_feed/bookmark'] = 'news_feed/main/my_bookmark_news_feed';
$route['api/news_feed/detail'] = 'news_feed/main/detail';
$route['api/news_feed/add'] = 'news_feed/main/add';
$route['api/news_feed/update'] = 'news_feed/main/update';
$route['api/news_feed/search'] = 'news_feed/main/search';

// route untuk fitur my_photo
$route['api/console/my_photo'] = 'my_photo/main';
$route['api/my_photo/input'] = 'my_photo/main/input';
$route['api/my_photo/get_photo_by_users_id'] = 'my_photo/main/get_photo_by_users_id';
$route['api/my_photo/detail'] = 'my_photo/main/detail';
$route['api/my_photo/register'] = 'my_photo/main/register';
$route['api/my_photo/update'] = 'my_photo/main/update';
$route['api/my_photo/login'] = 'my_photo/main/login';
$route['api/my_photo/search'] = 'my_photo/main/search';


// route untuk fitur diet_rules
$route['api/console/diet_rules'] = 'diet_rules/main';
$route['api/diet_rules/get'] = 'diet_rules/main/get';
$route['api/diet_rules/get_diet_collection'] = 'diet_rules/main/get_diet_collection';
$route['api/diet_rules/detail'] = 'diet_rules/main/detail';
$route['api/diet_rules/register'] = 'diet_rules/main/register';
$route['api/diet_rules/input_rules'] = 'diet_rules/main/input_rules';
$route['api/diet_rules/update'] = 'diet_rules/main/update';
$route['api/diet_rules/login'] = 'diet_rules/main/login';
$route['api/diet_rules/search'] = 'diet_rules/main/search';

// route untuk fitur collection_rules
$route['api/console/collection_rules'] = 'collection_rules/main';
$route['api/collection_rules/get'] = 'collection_rules/main/get';
$route['api/collection_rules/get_collection_users'] = 'collection_rules/main/get_collection_users';
$route['api/collection_rules/detail'] = 'collection_rules/main/detail';
$route['api/collection_rules/register'] = 'collection_rules/main/register';
$route['api/collection_rules/input_collection'] = 'collection_rules/main/input_collection';
$route['api/collection_rules/update'] = 'collection_rules/main/update';
$route['api/collection_rules/login'] = 'collection_rules/main/login';
$route['api/collection_rules/search'] = 'collection_rules/main/search';

/* End of file routes.php */
/* Location: ./application/config/routes.php */

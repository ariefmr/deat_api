<?php
	$item = '$item->';
	$config = Array(
					'form_contoh' => Array(
									'title' => Array( 
													'page' => 'Contoh Page',
													'panel' => 'Contoh Panel'
													),
									'form' => Array(
													'page' => 'Contoh Page',
													'panel' => 'Contoh Panel'
													)
									),
					'users_label' => Array(
									'title' => Array( 
													'page' => 'Users Console',
													'panel' => 'Users Console'
													),
									'form' => Array(
													'page' => 'User',
													'panel' => 'User'
													)
									),
					'diet_rules_label' => Array(
									'title' => Array( 
													'page' => 'Diet Rules Console',
													'panel' => 'Diet Rules Console'
													),
									'form' => Array(
													'page' => 'User',
													'panel' => 'User'
													)
									),
					'my_photo_label' => Array(
									'title' => Array( 
													'page' => 'My Photo Console',
													'panel' => 'My Photo Console'
													),
									'form' => Array(
													'path_foto' 				=> Array( 	'name' 	=> 'path_foto',
																					 		'label'	=> 'Foto Profil'
																						)
													)
									),
					'news_feed_label' => Array(
									'title' => Array( 
													'page' => 'News Feed Console',
													'panel' => 'News Feed Console'
													),
									'form' => Array(
													'path_foto' 				=> Array( 	'name' 	=> 'path_foto',
																					 		'label'	=> 'Foto Profil'
																						)
													)
									)					
			);
/* PETUNJUK PEMAKAIAN :

---Masih di controller---
Load dulu file config labels.php:
$this->load->config('labels');

Simpan ke data constant:
$data['constant'] = $this->config->item('form_pendok');
---Selesai di controller---

---Penggunaan Di file views---
// Menampilkan text untuk page title (biasanya di gunakan di templates header untuk judul page)
echo $constant['title']['page']; 

// Menampilkan text untuk judul panel
echo $constant['title']['panel'];

// Saat set attribut untuk form input, misal field Nomor Surat Permohonan
<label for="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"> 
<?php echo $constant['form']['no_surat_permohonan']['label'];?>
</label>
<input 
type="text" 
id="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
name="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
value=""/>

---Selesai di file views---
*/



/* TEMPLATE:
	// PERHATIKAN TANDA KOMA SEBELUM DAN SESUDAH ARRAY
					'form_kapal' => Array(
									'title' => Array( 
													'page' => 'Halaman Entry Kapal',
													'panel' => 'Entry Kapal'
													),
									'form' => Array(
													'nama_kapal' => Array( 'name' => 'nama_kapal',
																					'label'	=> 'Nama Kapal'
																				),
													'' => Array( 'name' => '',
																 'label' => ''
 																)
													)
									)
*/
?>
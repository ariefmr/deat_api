<?php
	$config = Array(
					'app' => Array(
		        					'header_title' => 'DEVNILA | Development New Interaction Learning and Application',
		        					'footer_text' => 'Hak cipta © 2013 | <strong>DEVNILA</strong> | Development New Interaction Learning and Application<br>
														Jl. Gagak No. 144 Sadang Serang, Bandung.'
		        					),
					'assets_paths' => Array(
	        						'misc_css' => base_url('assets/third_party/css'),
	        						'misc_js' => base_url('assets/third_party/js'),
	        						'misc_sounds' => base_url('assets/third_party/sounds'),
	        						'main_css' => base_url('assets/devnila/css'),
	        						'main_js' => base_url('assets/devnila/js'),
	        						'devnila_images' => base_url('assets/devnila/images'),
	        						'devnila_uploads' => base_url('assets/devnila/uploads'),
	        						'mockup_images' => base_url('assets/devnila/images/mockup')
	        						),
					'Hari_f'	=> Array('Senin', 'Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'),
					'Hari_s'	=> Array('Sen', 'Sel','Rab','Kam','Jum','Sab','Ming'),
					'Bulan_f' => Array('Januari', 'Februari', 'Maret',
											'April','Mei','Juni','Juli',
												'Agustus','September','Oktober','November','Desember'),
					'Bulan_s'	=> Array('Jan', 'Feb', 'Mar',
											'Apr','Mei','Jun','Jul',
												'Agst','Sept','Okt','Nov','Des')					
					);
?>